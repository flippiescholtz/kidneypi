#!/bin/sh
STARTED="1"
while true; do
SWITCH=$(gpio read 4)

if [ "$SWITCH" == "1" ]
then
	if [ "$STARTED" == "0" ]
	then
		echo "LAUNCHING...\n"
		STARTED="1"
		bash ./launch.sh
		sudo xset dpms force on
                sudo echo 255 > /sys/class/backlight/rpi_backlight/brightness
	fi
fi

if [ "$SWITCH" == "0" ]
then
	if [ "$STARTED" == "1" ]
	then
		echo "KILLING...\n"
		sudo killall -9 python
		sudo killall -9 kidneypi
		sudo xset dpms force off
		sudo echo 0 > /sys/class/backlight/rpi_backlight/brightness
		STARTED=0
	fi
fi
echo "Switch "$SWITCH"\n"
sleep 2
done
