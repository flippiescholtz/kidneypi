sudo killall -9 sudo python launch.py
sudo killall -9 kidneypi
sudo killall -9 ttymidi
sudo killall -9 timidity

echo "All stopped...\n"

sudo ./kidneypi_c/kidneypi &

echo "C program started...\n"

ttymidi -s /dev/ttyAMA0 -b 38400 &

echo "ttymidi started...\n"

timidity -B4,8 -iA &

echo "timidity started...\n"

echo "Sleeping so that c can catch up...\n"
sleep 2
echo "Connecting...\n"


echo "Starting Python..."

python ./kidneypi_python/launch.py &

sleep 4

echo "Connecting MIDI ports..."
aconnect -x
aconnect IntKB:0 LiveRouter:0
aconnect ttymidi:0 LiveRouter:1
aconnect LiveRouter:2 ttymidi:1
aconnect LiveRouter:2 TiMidity:0

echo "DONE with launch script"
#echo "Python started."
