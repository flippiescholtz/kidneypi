from kivy.config import Config
"""
This line is so that Kivy will display an on-screen keyboard for text input boxes
instead of relying on a USB hardware keyboard:
"""
Config.set('kivy', 'keyboard_mode', 'systemandmulti')
from kivy.app import App
from kivy.lang import Builder
from kivy.uix.screenmanager import ScreenManager, Screen
from login_screen import LoginScreen
from home_screen import HomeScreen
from sequencer_screen import SequencerScreen
from kivy.lang import Builder
from threading import RLock
import threading
from kivy.clock import Clock
import RPi.GPIO as GPIO
import time
from kivy.uix.popup import Popup
from kivy.uix.label import Label
from kivy.uix.button import Button
from eartraining.training_manager import TrainingManagerScreen
from functools import partial


"""
These classes form the entry point for the Kivy GUI.
Kivy must run in its own thread (it has its own event loop).
"""
class ParentScreen(App):
	def build(self):
		Clock.schedule_interval(self.check_gui_exit, 1)	
		return GUIManager.screenManager

	def check_gui_exit(self, data):
		if not GPIO.input(23):
			self.stop()

class GUIManager:	
	"""
	The ScreenManager class is provided by Kivy.
	It contains a reference to all the screens (Home screen, Sequencer screen, Exercise screens)
	and allows other code to switch between them when the particular feature is activated.
	"""
	screenManager = ScreenManager()

	"""
	The Builder.load_file calls make sure that Kivy has the screen layouts
	in memory so that it knows how to draw each screen.
	Once each screen is loaded, it is added to the screenManager
	so that we can switch to it later.
	"""
	Builder.load_file('login.kv')
	loginScreen = LoginScreen(name='login')
	screenManager.add_widget(loginScreen)
	Builder.load_file('home.kv')
	homeScreen = HomeScreen(name='home')
	screenManager.add_widget(homeScreen)
	Builder.load_file('sequencer.kv')
	sequencerScreen = SequencerScreen(name='sequencer')
	screenManager.add_widget(sequencerScreen)

	Builder.load_file('eartraining/layouts/training_manager.kv')
	trainingManagerScreen = TrainingManagerScreen(name='training_manager')
	screenManager.add_widget(trainingManagerScreen)

	parentScreen = ParentScreen()

	@classmethod
	def get_current_screen(self):
		return self.screenManager.current

	@classmethod
	def start(self):
		self.homeScreen.initialize()
		self.parentScreen.run()

	@classmethod
	def stop(self):
		try:
			App.stop()
		except:
			print("Error while closing the GUI.")

	"""
	This is called by other classes to open a particular screen as indicated
	by 'screenName'.
	screenName can be one of:
	'login', 'home', 'sequencer', 'training_manager'.
	"""
	@classmethod
	def show_screen(self, screenName, *args):
		Clock.schedule_once(partial(self.switch_screenmanager, screenName))
		
	"""
	Causes the screenManager to switch to the given screen.
	Scheduled from the show_screen method above.
	"""
	@classmethod
	def switch_screenmanager(self, screenName, *args):
		self.screenManager.current = screenName 
		if screenName == 'home':
			self.homeScreen.on_show()
		if screenName == 'login':
			self.loginScreen.on_show()
		if screenName == 'sequencer':
			self.sequencerScreen.on_show()

	"""
	This is called from the MainController class when the system starts up.
	It starts the whole Kivy GUI and shows the Login screen.
	"""
	@classmethod
	def run_gui(self):
		self.parentScreen = ParentScreen()	
		self.parentScreen.run()
		self.show_screen('login')

	"""
	This can be called by any other class to show a pop-up message
	with the given heading and body.
	"""
	@classmethod
	def show_popup_message(self, message, heading="", *args):
		content = Label(text=message)
		self.popup = Popup(title=heading, content=content)
		self.popup.open()
		Clock.schedule_once(self.dismiss_popup, 3)

	@classmethod
	def dismiss_popup(self, data):
		self.popup.dismiss()
