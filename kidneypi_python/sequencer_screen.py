from kivy.app import App
from kivy.lang import Builder
from kivy.uix.screenmanager import Screen, ScreenManager
from kivy.garden.knob import  Knob
import midi_sequencer
from kivy.clock import Clock
from kivy.properties import NumericProperty, ObjectProperty
from functools import partial
import file_dialogs
from kivy.uix.popup import Popup
import os
from session_manager import SessionManager 

"""
This Kivy screen is shown on the LCD when the multi-track sequencer is being used.
It manages the user interface for the sequencer.
The corresponding kivy layout can be found in the file 'sequencer.kv'.
"""
class SequencerScreen(Screen):

	trackNumber = 1 # By default, the LCD shows track 1's details.

	"""
	This property is bound to the rotary encoder control on the LCD.
	NumericProperty is a class provided by kivy which allows automatic updating
	of the volume variable when the 'knob' control on the LCD changes. 
	"""
	volume = NumericProperty(127)

	def __init__(self, **kwargs):
		super(SequencerScreen, self).__init__(**kwargs)
		self.mute = False # By default, the current track is not muted or solo'd.
		self.solo = False
		"""
		Here we bind the 'volume' variable to the knob on the GUI,
		so that the variable automatically gets updated when the knob is turned.
		The knob control is defined in the 'sequencer.kv' layout file.
		"""
		self.bind(volume = self._knob_touched)		

	"""
	This is called when the screen is shown to the user.
	It makes sure that the latest track data is shown on the GUI.
	"""
	def on_show(self):
		# We schedule the method call to make sure it runs on the UI thread and no other thread.
		Clock.schedule_once(self.update_gui)

	"""
	Changes the currently selected track number,
	and updates the GUI with the new track's information.
	"""
	def select_track(self, track_number):
		self.trackNumber = track_number
		Clock.schedule_once(self.update_gui)
		
	"""
	Called when the volume knob on the GUI is turned.
	"""
	def _knob_touched(self, instance, value):
		self.set_volume(value, False)
		
	"""
	This method updates all GUI fields and controls with the latest data
	for the currently selected track.
	"""
	def update_gui(self, *args):
		"""
		Get the currently selected track's status from the MidiSequencer class.
		e.g. what is the current song's filename, is the current track armed, etc.
		This is displayed to the user by setting the labels' text properties.
		"""
		self.ids.lbl_song_name.text = midi_sequencer.MidiSequencer.patternName 
		self.ids.lbl_track_heading.text = "Track " + str(self.trackNumber) + " settings:"
		self.ids.lbl_armed_internal.text = str(midi_sequencer.MidiSequencer.armedTrackNumbers[0] + 1) if midi_sequencer.MidiSequencer.armedTrackNumbers[0] >= 0 else 'None'
		self.ids.lbl_armed_external.text = str(midi_sequencer.MidiSequencer.armedTrackNumbers[1] + 1) if midi_sequencer.MidiSequencer.armedTrackNumbers[1] >= 0 else 'None'
		self.ids.lbl_track_instrument.text = midi_sequencer.MidiSequencer.TRACK_PATCHNAMES[self.trackNumber - 1]

		if self.trackNumber - 1 == midi_sequencer.MidiSequencer.armedTrackNumbers[0]:
			self.ids.lbl_track_status.text = "Armed on Internal keyboard"
		elif self.trackNumber - 1 == midi_sequencer.MidiSequencer.armedTrackNumbers[1]:
			self.ids.lbl_track_status.text = "Armed on External keyboard"

		self.ids.lbl_status.text = midi_sequencer.MidiSequencer.get_status()
		self.update_volume_gui() # Make sure the volume control shows the correct value.

		"""
		If the selected track is muted or solo'd, show it in the GUI by 
		highlighting the appropriate button(s).
		"""
		self.mute = midi_sequencer.MidiSequencer.TRACK_MUTES[self.trackNumber-1]
		self.solo = midi_sequencer.MidiSequencer.TRACK_SOLOS[self.trackNumber-1]
		self.update_mute_solo_gui()
	
	"""
	Gets the currently selected track's volume from the MIDI sequencer class,
	and updates the knob control on the GUI to match.
	"""
	def update_volume_gui(self, update_knob = True, *args):
		vol = midi_sequencer.MidiSequencer.TRACK_VOLUMES[self.trackNumber-1]
		self.ids.lbl_volume.text = str(vol)
		if update_knob:
			self.volume = float(vol)

	"""
	Highlights the appropriate button(s) based on the current track's 'mute' or 'solo' states.
	"""
	def update_mute_solo_gui(self):
		self.ids.btn_mute.background_color = (1, 0, 0, 1) if self.mute else (0.6,0.6,0.6,1)
		self.ids.btn_mute.state = 'down' if self.mute else 'normal'

		self.ids.btn_solo.background_color = (1, 0, 0, 1) if self.solo else (0.6,0.6,0.6,1)
		self.ids.btn_solo.state = 'down' if self.solo else 'normal'


	"""
	Toggles the 'mute' state on the current track. 
	Called when the Mute button is pressed on the LCD.
	"""
	def mute_pressed(self, *args):
		if self.ids.btn_solo.state == 'down':
			# A track cannot be muted while it is solo'd.
			return
		self.mute = not self.mute
		"""
		The MidiSequencer class actually sends the MIDI messages to enforce the mute,
		so we notify it that the track's mute state has changed.
		"""
		midi_sequencer.MidiSequencer.set_track_mute(self.trackNumber, self.mute)
		self.update_mute_solo_gui() # Make sure the buttons reflect the current mute / solo state.


	"""
	Toggles the 'solo' state on the current track.
	Same process as in the 'mute_pressed' method above.
	"""
	def solo_pressed(self, *args):
		if self.ids.btn_mute.state == 'down':
			return
		self.solo = not self.solo
		midi_sequencer.MidiSequencer.set_track_solo(self.trackNumber, self.solo)
		self.update_mute_solo_gui()


	"""
	Called when the leftmost rotary encoder is turned one step.
	'steps' can be positive (increase volume) or negative (decrease volume).
	"""
	def increment_volume(self, steps):
		midi_sequencer.MidiSequencer.increment_volume(self.trackNumber, steps)
		Clock.schedule_once(self.update_volume_gui)
	
	"""
	Sets the current track's volume to a specific value
	rather than incrementing it. 
	"""
	def set_volume(self, val, update_knob=True):
		midi_sequencer.MidiSequencer.set_volume(self.trackNumber, val)
		Clock.schedule_once(partial(self.update_volume_gui, update_knob))

	"""
	The following variables and methods are used for the Load / Save dialog boxes
	for MIDI files. They are shown whenever the 'Load' or 'Save' buttons are pressed
	on the front panel.
	The dialogs are defined in 'file_dialogs.py'.
	"""

	loadfile = ObjectProperty(None)
	savefile = ObjectProperty(None)
	text_input = ObjectProperty(None)

	def dismiss_popup(self):
        self._popup.dismiss()

    """
    Called when the 'load' button is pressed on the front panel.
    Shows a file chooser dialog box, open at the currently logged-in user's directory.
    """
    def show_load(self):
       	content = file_dialogs.LoadDialog(load=self.load, cancel=self.dismiss_popup)
       	# By default, show the currently logged-in user's directory.
       	content.ids.filechooser.path = "/home/pi/patterns/" + SessionManager.get_user_name().lower()
       	self._popup = Popup(title="Load file", content=content,
                           size_hint=(0.9, 0.9))
       	self._popup.open()

    """
    Called when the 'save' button is pressed on the front panel.
    The user can type the name of their new file.
    By default, the file chooser shows the currently logged-in user's directory.
    """
   	def show_save(self):
       	content = file_dialogs.SaveDialog(save=self.save, cancel=self.dismiss_popup)
       	content.ids.filechooser.path = "/home/pi/patterns/" + SessionManager.get_user_name().lower()
       	self._popup = Popup(title="Save file", content=content,
                            size_hint=(0.9, 0.9))
       	self._popup.open()

    """
    Called when the user has picked a file from the 'Load File' dialog.
    This actually instructs the MidiSequencer to open the file and get it ready for playing.
    Some better error-handling could be implemented in future versions, e.g. if the file
    is corrupt.
    """
    def load(self, path, filename):
    	midi_sequencer.MidiSequencer.load_pattern(os.path.join(path, filename[0]))
       	midi_sequencer.MidiSequencer.patternName = os.path.basename(filename[0])
    	Clock.schedule_once(self.update_gui)
       	self.dismiss_popup()

    """
    Called when the user has confirmed their new file name in the 'Save File'
    dialog. This actually instructs the MidiSequencer to save the file.
    Some better error handling could be implemented in future versions (e.g. if the disk is full).
    """
    def save(self, path, filename):
    	midi_sequencer.MidiSequencer.save_pattern(os.path.join(path, filename))
    	midi_sequencer.MidiSequencer.patternName = filename
    	Clock.schedule_once(self.update_gui)
       	self.dismiss_popup()
