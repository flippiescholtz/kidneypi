import midi
import midi.sequencer as sequencer
import time
import threading

"""
This class communicates with the C++ KeyboardLedDriver.cpp class.
Whenever Python wants to turn on / off a keyboard LED, it calls this class.
This class then sends a MIDI message to the C++ class over a virtual ALSA MIDI port,
which causes C++ to modify the LED state over the hardware I2C bus.
"""
class KeyboardLedSender:
	
	@classmethod
	def initialize(self):
		"""
		Connect to the ALSA virtual MIDI port registered by the C++ class, 
		so that we can send MIDI 'notes' to it that change the LED states. 
		"""
		self.seq = sequencer.SequencerWrite(sequencer_name='kidneypi_led_sequencer')
		hardware = sequencer.SequencerHardware()
		outputClient = hardware.get_client("IntKBLeds")
	    	outputPort = outputClient.get_port("IntKBLedsIn")
		self.seq.subscribe_port(outputClient.client, outputPort.port)
		lightshowThread = threading.Thread(target=self.start_lightshow)
		lightshowThread.start()

	"""
	Triggers the keyboard lightshow, as defined in the lightshow method below.
	"""
	@classmethod
	def start_lightshow(self):
		self.lightshow(4) # play the sequence 4 times.

	"""
	The 'lightshow' is a flourish that plays on the keyboard LEDs when the system
	starts up. It lights up each LED in sequence, showing that all keyboard LEDs are working.
	
	Two LEDs are on at a time, starting at Middle C and moving outwards towards either end
	of the keyboard, and then back. This is repeated for the number specified in 'count'.
	"""
	@classmethod
	def lightshow(self, count):
		for i in range(count):
			leftPos = 30
			rightPos = 30
			while leftPos >= 0 and rightPos <= 60:
				self.led_on(36+leftPos)
				self.led_on(36+rightPos)
				time.sleep(0.2)
				self.led_off(36+leftPos)
				self.led_off(36+rightPos)
				leftPos -= 1
				rightPos += 1

			leftPos = 0
			rightPos = 60

			while leftPos <= 30 and rightPos >= 30:
				self.led_on(36+leftPos)
				self.led_on(36+rightPos)
				time.sleep(0.2)
				self.led_off(36+leftPos)
				self.led_off(36+rightPos)
				leftPos += 1
				rightPos -= 1


	"""
	Called by another Python class when it wants to turn on a keyboard LED.
	Currently, only the Chord Matching exercise uses this.
	"""
	@classmethod
	def led_on(self, noteNumber):
		# The C++ class will interpret a 'Note On' event as 'turn on the LED'
		ev = midi.NoteOnEvent(pitch=noteNumber, velocity=127)
		buf = self.seq.event_write(ev, True, False, False)
		self.seq.drain()

	"""
	Called by another Python class when it wants to turn off a keyboard LED.
	Currently, only the Chord Matching exercise uses this.
	"""
	@classmethod
	def led_off(self, noteNumber):
		# The C++ class will interpret a 'Note Off' event as 'turn off the LED'
		ev = midi.NoteOnEvent(pitch=noteNumber, velocity=0)
		buf = self.seq.event_write(ev, True, False, False)
		self.seq.drain()

	"""
	Convenience method to turn all keyboard LEDs off at once.
	"""
	@classmethod
	def all_off(self):
		for i in range(36, 36+61):
			self.led_off(i)

	"""
	Convenience method to turn all keyboard LEDs on at once.
	This was used for testing the hardware.
	"""
	@classmethod
	def all_on(self):
		for i in range(36, 36+61):
			self.led_on(i)

	"""
	Convenience method for turning on the LEDs for a list of note numbers.
	This is used by the chord matching exercise (a chord is a list of notes).
	"""
	@classmethod
	def list_on(self, noteNumbers):
		for n in noteNumbers:
			self.led_on(n)


