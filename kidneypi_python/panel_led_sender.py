import time 
import math 
import spidev
import threading
import RPi.GPIO as GPIO

"""
This class communicates directly with the MAX7219-based LED driver module over SPI.
It keeps the status of every LED on the front panel, and sends the appropriate SPI
commands the LED module whenever an LED state changes.
This class is called by any other class that wants to turn an LED on or off.
"""
class PanelLedSender:
	# constants for each LED to define their [row, column] position in the LED matrix.
	# they are ordered left to right, top to bottom according to the LED matrix.
	SOUND_1_RED = [0,0]
	SOUND_1_GREEN = [0,1]
	SOUND_2_RED = [0,2]
	SOUND_2_GREEN = [0,3]
	SOUND_3_RED = [0,4]
	SOUND_3_GREEN = [0,5]
	SOUND_KBD_INT = [0,6]
	SOUND_4_RED = [1,0]
	SOUND_4_GREEN = [1,1]
	SOUND_5_RED = [1,2]
	SOUND_5_GREEN = [1,3]
	SOUND_6_RED = [1,4]
	SOUND_6_GREEN = [1,5]
	SOUND_KBD_EXT = [1,6]
	SOUND_7_RED = [2,0]
	SOUND_7_GREEN = [2,1]
	SOUND_8_RED = [2, 2]
	SOUND_8_GREEN = [2,3]
	SOUND_9_RED = [2,4]
	SOUND_9_GREEN = [2, 5]
	PART_LOWER = [2,6]
	PLAY_RED = [3,0]
	PLAY_GREEN = [3,1]
	SOUND_0_RED = [3,2]
	SOUND_0_GREEN = [3,3]
	REC_RED = [3,4]
	REC_GREEN = [3,5]
	PART_UPPER1 = [3,6]
	TRACK_1_RED = [4,0]
	TRACK_1_GREEN = [4,1]
	TRACK_2_RED = [4,2]
	TRACK_2_GREEN = [4,3]
	TRACK_3_RED = [4,4]
	TRACK_3_GREEN = [4,5]
	PART_UPPER2 = [4,6]
	TRACK_4_RED = [5,0]
	TRACK_4_GREEN = [5,1]
	TRACK_5_RED = [5,2]
	TRACK_5_GREEN = [5,3]
	TRACK_6_RED = [5,4]
	TRACK_6_GREEN = [5,5]
	LOGIN = [5,6]
	TRACK_KB_SELECT_A = [6,0]
	TRACK_KB_SELECT_B = [6,1]
	TRACK_7_RED = [6,2]
	TRACK_7_GREEN = [6,3]
	TRACK_8_RED = [6,4]
	TRACK_8_GREEN = [6,5]
	START_RED = [7,0]
	START_GREEN = [7,1]


	# register values for the MAX7219:
	MAX7219_REG_NOOP = 0x0
	"""
	The 'digit' registers correspond to our matrix rows,
	because we are using the MAX7219 in matrix mode rather than seven-segment mode.
	"""
	MAX7219_REG_DIGIT0 = 0x1
	MAX7219_REG_DIGIT1 = 0x2
	MAX7219_REG_DIGIT2 = 0x3
	MAX7219_REG_DIGIT3 = 0x4
	MAX7219_REG_DIGIT4 = 0x5
	MAX7219_REG_DIGIT5 = 0x6
	MAX7219_REG_DIGIT6 = 0x7
	MAX7219_REG_DIGIT7 = 0x8
	MAX7219_REG_DECODEMODE = 0x9
	MAX7219_REG_INTENSITY = 0xA
	MAX7219_REG_SCANLIMIT = 0xB
	MAX7219_REG_SHUTDOWN = 0xC
	MAX7219_REG_DISPLAYTEST = 0xF

	# The locks ensure that only one thread at a time can change the LED states.
	lock = threading.RLock()
	pulseLock = threading.RLock()

	# Each matrix row has an integer representing the state of its columns.
	# Each bit in the integer corresponds to another column.
	led_states = [0,0,0,0,0,0,0]

	"""
	'flashingFlags' stores whether each LED is currently flashing or not.
	Any LED can be flashed at about 2.5 Hz by setting its flashingFlag to true. 
	"""
	flashingFlags = [x[:] for x in [[False] * 7] * 8]
	flashingLock = threading.RLock()
	flashingThreadRunning = False
	spiOpen = False

	"""
	Called from the MainController class when everything starts up.
	Initializes the SPI connection to the MAX7219 and starts the threads.
	"""
	@classmethod
	def start(self):
		self._spi = spidev.SpiDev()
		self._spi.open(0,0) # The MAX7219 is connected the the Pi's SPI port 0.
		self._spi.xfer2(list([self.MAX7219_REG_SCANLIMIT, 7])) # use all 8 rows
		self._spi.xfer2(list([self.MAX7219_REG_DECODEMODE, 0]))   # use matrix (not digits)
		self._spi.xfer2(list([self.MAX7219_REG_DISPLAYTEST, 0]))  # no display test
		self._spi.xfer2(list([self.MAX7219_REG_SHUTDOWN, 1]))     # not shutdown mode
		self._spi.xfer2(list([self.MAX7219_REG_INTENSITY, 15])) # brightness
		self.spiOpen = True
		self.flashingThread = threading.Thread(target=self.perform_led_flashes)
		self.flashingThreadRunning = True
		self.flashingThread.start()

	@classmethod
	def stop(self):
		if self.flashingThreadRunning:
			self.flashingThreadRunning = False;
			self.flashingThread.join()
		self._spi.close()
		self.spiOpen = False

	"""
	Sends the current status of each LED to the MAX7219 using SPI.
	"""
	@classmethod
	def update(self):
		if not GPIO.input(23) or not self.spiOpen:
			# the LED module's 5V supply is turned off, so the SPI device won't respond.
			return
		self.lock.acquire()
		# For each row, send its current integer value (column bits) to the MAX7219.
		for col in range(7):
			self._spi.xfer2(list([self.MAX7219_REG_DIGIT0 + col, self.led_states[col]]))
		self.lock.release()

	"""
	Called by other classes to turn an LED on.
	The 'led' variable is one of the LED name constants defined at the top
	of this class.
	"""
	@classmethod
	def led_on(self, led):
		self.lock.acquire()
		row = led[0]
		col = led[1]
		# Set the appropriate bit on the appropriate row value to make this LED turn on.
		self.led_states[col] = self.set_bit(self.led_states[col], row)
		self.update()
		self.lock.release()
		
	"""
	Called by other classes to turn an LED off.
	The 'led' variable is one of the LED name constants defined at the top
	of this class.
	"""
	@classmethod
	def led_off(self, led):
		self.lock.acquire()
		row = led[0]
		col = led[1]
		# Set the appropriate bit on the appropriate row value to make this LED turn off.
		self.led_states[col] = self.clear_bit(self.led_states[col], row)
		self.update()
		self.lock.release()

	"""
	A convenient shortcut for calling led_on or led_off.
	"""
	@classmethod
	def set_led(self, led, isOn):
		if isOn:
			self.led_on(led)
		else:
			self.led_off(led)


	"""
	Turns off all the LEDs in the 'Sound Select' section of the front panel.
	Used when a new sound is selected.
	"""
	@classmethod
	def sound_leds_off(self):
		# numbers 1 - 9:
		for row in range(3):
			for col in range(6):
				self.led_off([row,col])
		# now the zero:
		self.led_off(self.SOUND_0_RED)
		self.led_off(self.SOUND_0_GREEN)

	"""
	Turns of all the 'track' LEDs on the front panel (tracks 1 - 8)
	"""
	@classmethod
	def track_leds_off(self):
		for row in range(4, 6):
			for col in range(6):
				self.led_off([row, col])
		for col in range(2, 6):
			self.led_off([6, col])

	"""
	Turns on the red LED at the particular Sound Select number.
	The 'number' parameter is between 1 and 9.
	"""
	@classmethod
	def show_sound_led(self, number):
		self.lock.acquire()
		self.sound_leds_off()
		number = number - 1
		if number < 0: # the zero LED
			row = 3
			col = 2
		else:
			row = number // 3
			col = (number % 3) * 2
		self.led_on([row,col])
		self.lock.release()

	"""
	Converts a track number (1 - 8) to its corresponding LED constant value,
	as defined at the top of this file.
	The 'is_red' variable determines whether the red or green cathod of the bi-colour
	LED is returned.
	"""
	@classmethod
	def track_number_to_led(self, track_number, is_red):
		if track_number == 1:
			return self.TRACK_1_RED if is_red else self.TRACK_1_GREEN
		if track_number == 2:
			return self.TRACK_2_RED if is_red else self.TRACK_2_GREEN
		if track_number == 3:
			return self.TRACK_3_RED if is_red else self.TRACK_3_GREEN
		if track_number == 4:
			return self.TRACK_4_RED if is_red else self.TRACK_4_GREEN
		if track_number == 5:
			return self.TRACK_5_RED if is_red else self.TRACK_5_GREEN
		if track_number == 6:
			return self.TRACK_6_RED if is_red else self.TRACK_6_GREEN
		if track_number == 7:
			return self.TRACK_7_RED if is_red else self.TRACK_7_GREEN
		if track_number == 8:
			return self.TRACK_8_RED if is_red else self.TRACK_8_GREEN
		return self.TRACK_1_RED # default if all else fails

	"""
	Turns on the LED at the given track_number (1 - 8).
	If 'is_red' is true, the red cathode of the bi-colour LED is turned on,
	otherwise the green.
	"""
	@classmethod
	def track_led_on(self, track_number, is_red=False):
		self.lock.acquire()
		self.led_on(self.track_number_to_led(track_number, is_red))
		self.lock.release()

	"""
	Turns off all LEDs on the front panel.
	"""
	@classmethod
	def all_off(self):
		self.lock.acquire()
		for col in range(7):
			self.led_states[col] = 0
		self.update()
		self.lock.release()

	"""
	Turns on all LEDs on the front panel.
	This was used for testing.
	"""
	@classmethod
	def all_on(self):
		self.lock.acquire()
		for col in range(7):
			self.led_states[col] = 255
		self.update()
		self.lock.release()

	"""
	This causes all LEDs on the front panel to 'pulse' the given number of times.
	It varies the duty cycle of all LEDs using the Intensity register on the MAX7219,
	creating a 'pulsing' or 'fading' effect.
	This is called when the main switch is turned on, as a sort of 'lightshow'
	to greet the user and to confirm that the LEDs are working.
	"""
	@classmethod
	def pulse_all(self, count):
		self.lock.acquire()
		self.all_on()
		for i in range(count):
			for brightness in range(15):
				self._spi.xfer2(list([self.MAX7219_REG_INTENSITY, brightness]))
				time.sleep(0.013)
			for brightness in range(15):
				self._spi.xfer2(list([self.MAX7219_REG_INTENSITY, 15-brightness]))
				time.sleep(0.013)
		self.all_off()
		self._spi.xfer2([self.MAX7219_REG_INTENSITY, 15])
		self.lock.release()

	"""
	When this is called, the given LED will start flashing at about 2.5 Hz.
	This is used, to flash the 'Lower' LED when the system is waiting for a split point
	to be input.
	The actual flashing is done in its own separate thread.
	The 'led' parameter is one of the LED constants defined at the top of this file.
	"""
	@classmethod
	def start_flashing_led(self, led):
		self.flashingLock.acquire()
		self.flashingFlags[led[0]][led[1]] = True
		self.flashingLock.release()

	"""
	When this is called, the given LED stops flashing.
	The 'led' parameter is one of the LED constants defined at the top of this file.
	"""
	@classmethod
	def stop_flashing_led(self, led):
		self.flashingLock.acquire()
		self.flashingFlags[led[0]][led[1]] = False
		self.flashingLock.release()


	"""
	This method runs in its own separate thread.
	It flashes all LEDs for which the flashingFlags entry is true.
	Flashing happens at about 2.5 Hz.
	"""
	@classmethod
	def perform_led_flashes(self):
		state = True
		self.pulse_all(9)
		while self.flashingThreadRunning:
			self.flashingLock.acquire()
			for row in range(8):
				for col in range(7):
					if self.flashingFlags[row][col] == True:
						if state:
							self.led_on([row, col])
						else:
							self.led_off([row, col])
			state = not state
			self.flashingLock.release()
			# The state of all flashing LED's are toggled at 2.5 Hz.
			time.sleep(0.4)

	"""
	Sets a bit in the given integer value.
	"""
	@classmethod
	def set_bit(self, value, bit):
    		return value | (1<<bit)

    """
	Clears a bit in the given integer value.
	"""
	@classmethod
	def clear_bit(self, value, bit):
	    	return value & ~(1<<bit)





		
	
