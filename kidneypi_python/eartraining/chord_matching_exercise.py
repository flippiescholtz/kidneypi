from exercise import Exercise, ExerciseStates
import random
import mingus.core.intervals as intervals
import time
from mingus.containers import Note
import midi_sequencer
import threading
from kivy.clock import Clock
from kivy.uix.popup import Popup
from kivy.uix.label import Label
from kivy.logger import Logger
from kivy.lang import Builder
from keyboard_led_sender import KeyboardLedSender
import mingus.core.chords as chords
from music_libs import MusicLibs
import midi
import midi.sequencer as sequencer

"""
This class implements the Chord Recognition exercise.
See chapter 4.6 in the report for a description of this exercise.
"""
class ChordMatchingExercise(Exercise):
	
	def __init__(self, name):
		super(ChordMatchingExercise,self).__init__(name=name)
		self.exerciseName = 'chord_matching'
		self.levelIntroText = []
		self.levelIntroText.append("In this level, you only need to match the type of the chord.")
		self.levelIntroText.append("In this level, you need to match the type and inversion (root, 1st or second)")
		self.levelIntroText.append("In this level, you need to match the name of the chord as well. Only for advanced students!")

		self.chordTypes = ["major", "minor", "augmented", "diminished", "suspended", "minor_seventh"]

		# This chord is displayed on the keyboard's LEDs.
		self.displayChord = ["C", "E", "G"] # Default to C major triad

		# This list of chords is played alternately while the LED chord is displayed.
		# The user must match one of these chords with the one in displayChord.
		self.playbackChords = [["C", "E", "G"], ["C", "Eb", "G"]]
		self.currentPlaybackChord = []

	"""
	Generates a random chord based on the complexity rules for the current level.
	"""
	def random_chord_for_level(self):
		chordType = random.choice(self.chordTypes)
		inversion = 0
		keyName = "C"
		
		if self.level > 1:
			inversion = random.randint(0, 2)
		if self.level > 2:
			n = Note()
			n.from_int(60 + random.randint(0,12))
			keyName = n.name

		return self.get_chord_by_inversion(keyName, chordType, inversion)

	"""
	Builds the chord with given name, type and inversion.
	Returns the list of notes of which the chord is comprised.
	"""
	def get_chord_by_inversion(self, keyName="C", chordType="major", inversion=0):
		if chordType == "minor":
			chord = chords.minor_triad(keyName)
		elif chordType == "augmented":
			chord = chords.augmented_triad(keyName)
		elif chordType == "diminished":
			chord = chords.diminished_triad(keyName)
		elif chordType == "suspended":
			chord = chords.suspended_triad(keyName)
		elif chordType == "minor_seventh":
			chord = chords.minor_seventh(keyName)
		else:
			chord = chords.major_triad(keyName)

		if inversion == 1:
			chord = chords.first_inversion(chord)
		elif inversion == 2:
			chord = chords.second_inversion(chord) 

		return chord

	"""
	Generate a new question,
	complete with LED chord and list of audible chords.
	"""
	def generate_random_question(self):
		numPlaybackChords = self.level * 2

		self.playbackChords = []
		self.displayChord = self.random_chord_for_level()
		
		self.playbackChords.append(self.displayChord)
		for i in range(numPlaybackChords):
			chord = self.random_chord_for_level()
			while chord in self.playbackChords:
				chord = self.random_chord_for_level()
			self.playbackChords.append(chord)

		random.shuffle(self.playbackChords)
		
	"""
		Play the most recently generated question.
	"""
	def play_question(self):
		# Show the displayChord on the keyboard LEDs
		KeyboardLedSender.all_off()
		displayNoteNumbers = MusicLibs.note_names_to_numbers(self.displayChord)
		KeyboardLedSender.list_on(displayNoteNumbers)
		
		# Start listening for a keypress (answer)
		self.answerListenThread = threading.Thread(target=self.listen_for_answer)
		self.answerListenThread.start()

		# Play the list of 'audible' chords over and over until a keypress is received.
		while self.state == ExerciseStates.PLAYING_QUESTION and not self.cancelCurrentQuestion:
			for i in range(0, len(self.playbackChords)):
				if self.state != ExerciseStates.PLAYING_QUESTION:
					# The exercise has been stopped from another thread, or the user has answered.
					break
				self.currentPlaybackChord = self.playbackChords[i]
				playbackNoteNumbers = MusicLibs.note_names_to_numbers(self.playbackChords[i])
				midi_sequencer.MidiSequencer.play_chord(playbackNoteNumbers, 2)
				if self.cancelCurrentQuestion:
					# The exercise has been stopped from another thread.
					break
				time.sleep(2) # Give the user two seconds to decide before playing the next chord.
		# The user has answered. Turn off all the keyboard LEDs.
		KeyboardLedSender.all_off()
		self.answerListenThread.join()

	"""
	Wait for any keypress from the internal or external MIDI keyboard.
	If the keypress happens while the LED chord is also being played audibly,
	the answer is marked correct.
	"""
	def listen_for_answer(self):
		# Connect to the virtual MIDI ports for the internal keyboard and external MIDI keyboard.
		self.seq = sequencer.SequencerRead(sequencer_name='kidneypi_chordmatch_sequencer')
		hardware = sequencer.SequencerHardware()
		
		inputClientInternal = hardware.get_client("IntKB")
		inputPortInternal = inputClientInternal.get_port("IntKBOut")
		inputClientExternal = hardware.get_client("ttymidi")
		inputPortExternal = inputClientExternal.get_port("MIDI out")

		self.seq.subscribe_port(inputClientInternal.client, inputPortInternal.port)
		self.seq.subscribe_port(inputClientExternal.client, inputPortExternal.port)

		while self.state == ExerciseStates.PLAYING_QUESTION and not self.cancelCurrentQuestion:
			# Check if a MIDI Note On message has arrived. Non-blocking.
			event = self.seq.event_read()
			if event is not None and isinstance(event, midi.NoteOnEvent) and event.velocity > 0:
				print("Event found!")
				self.state = ExerciseStates.PLAYING_RESULT
				self.check_answer()
			time.sleep(0.5) # Check every half second.



	def wrong_answer(self, *args):
		self.ids.tab_panel.switch_to(self.ids.item_result)
		self.ids.lbl_result_heading.text = "Oops"
		self.ids.lbl_result_body.text = "That answer is not correct. Let's try again."
		if self.level == 3 and self.points >= 3:
			self.subtract_points(self.level)
		else:
			self.add_points(0)

	def correct_answer(self, *args):
		self.ids.tab_panel.switch_to(self.ids.item_result)
		self.ids.lbl_result_heading.text = "Well done!"
		self.ids.lbl_result_body.text = "You are correct. Let's try another one."
		self.add_points(self.level)


	def check_answer(self):
		if self.cancelCurrentQuestion:
			return
		if self.currentPlaybackChord == self.displayChord:
			# User pressed the key at the right time.
			# Schedule the correct_answer method on the UI thread,
			# because the UI cannot be manipulated from other threads.
			Clock.schedule_once(self.correct_answer, 0.5)
		else:
			Clock.schedule_once(self.wrong_answer, 0.5)
		# This question is now done. Start with the next question...
		Clock.schedule_once(self.start_next_question, 3)
		
		
