from exercise import Exercise
import random
import mingus.core.intervals as intervals
import time
from mingus.containers import Note
import midi_sequencer
import threading
from kivy.clock import Clock
from kivy.uix.popup import Popup
from kivy.uix.label import Label
from kivy.logger import Logger
from kivy.uix.togglebutton import ToggleButton
from music_libs import MusicLibs
from relative_pitch_exercise import RelativePitchExercise
from kivy.lang import Builder

"""
Implementation of the Interval Exercise (see chapter 4.4 in the report).
The user hears two notes and has to choose the name of the interval.

It is an extension of the Relative Pitch exercise.
"""
class IntervalExercise(RelativePitchExercise):
	
	def __init__(self, name):
		super(IntervalExercise,self).__init__(name=name)
		
		self.exerciseName = 'interval'
		self.levelIntroText = []
		self.levelIntroText.append("In this level, only perfect intervals and the major third are asked.")
		self.levelIntroText.append("In this level, the minor third and sixth are added.")
		self.levelIntroText.append("In this level, all possible intervals are asked.")

		# Define which intervals are played in each level.
		# These interval name strings are fed into the mingus library when the answers are computed.
		# e.g. "4" is a perfect fourth, "b3" is a minor third, etc.
		self.intervalsByLevel = []
		self.intervalsByLevel.append(["4", "5", "3", "8"]) # level 1
		self.intervalsByLevel.append(["4", "5", "3", "8", "6", "2", "b3", "b6"]) # level 2
		self.intervalsByLevel.append(["4", "5", "3", "8", "6", "2", "b3", "b6", "7", "b7", "b2", "b5"]) # level 3

	def answer_chosen(self, answer):
		checkAnswerThread = threading.Thread(target=self.check_answer)
		checkAnswerThread.start()

	"""
	Checks whether the user picked the correct answer by comparing the selected interval
	with the one in the question.
	"""
	def check_answer(self):
		# Get the selected answer from the UI buttons selected on the LCD:
		tonalityWidgets = [t for t in ToggleButton.get_widgets('tonality') if t.state =='down']
		distanceWidgets = [t for t in ToggleButton.get_widgets('interval') if t.state =='down']

		if len(tonalityWidgets) == 0 or len(distanceWidgets) == 0:
			# User hasn't selected an answer.
			return
		
		# Convert the selected interval name into a string that mingus can understand, so that we can compare the distances.
		selectedInterval = "{} {}".format(tonalityWidgets[0].text, distanceWidgets[0].text)

		# Intervals are always measured from the bottom note, so we want to do all our
		# computations relative to the lower note, regardless of whether it's the first or second note:
		if int(self.intervalNote) >= int(self.referenceNote):
			topNote = self.intervalNote
			bottomNote = self.referenceNote
		else:
			topNote = self.referenceNote
			bottomNote = self.intervalNote

		# Ask the mingus library what the most appropriate name is for the interval defined by the two notes.
		# This is shown to the user if they answer incorrectly, so that they can learn from their mistakes.
		self.correctAnswer = MusicLibs.get_most_correct_interval(bottomNote, topNote)

		# Ask the mingus library if the selected interval name is valid for the two interval notes:
		if MusicLibs.is_interval_valid(bottomNote, topNote, selectedInterval):
			# User answered correctly
			Clock.schedule_once(self.correct_answer, 0.5)
		else:
			Clock.schedule_once(self.wrong_answer, 0.5)
		Clock.schedule_once(self.start_next_question, 3)

	"""
	Resets the state of the UI toggle buttons for a new question.
	"""
	def clear_answer(self):
		for t in ToggleButton.get_widgets('tonality'):
			t.state = 'normal'
		for t in ToggleButton.get_widgets('interval'):
			t.state = 'normal'