from exercise import Exercise
import random
import mingus.core.intervals as intervals
import time
from mingus.containers import Note
import midi_sequencer
import threading
from kivy.clock import Clock
from kivy.uix.popup import Popup
from kivy.uix.label import Label
from kivy.logger import Logger
from kivy.lang import Builder
from music_libs import MusicLibs


class RhythmExercise(Exercise):
	
	def __init__(self, name):
		
		super(RhythmExercise,self).__init__(name=name)
		self.exerciseName = 'rhythm'
		self.levelIntroText = []
		self.levelIntroText.append("In this level, you have to remember ONE bar.")
		self.levelIntroText.append("In this level, you have to remember TWO bars.")
		self.levelIntroText.append("In this level, you have to remember FOUR bars.")
		

	def play_question(self):
		numBars = self.level if self.level < 3 else 4
		self.correctRhythm = midi_sequencer.MidiSequencer.record_input_segment(numBars)
		time.sleep(2)
		#midi_sequencer.MidiSequencer.start_playback(self.correctRhythm, numBars)
		#time.sleep(5)
		Clock.schedule_once(self.ask_for_answer, 0)

	def ask_for_answer(self, *args):
	#	self.state = ExerciseStates.WAITING_FOR_ANSWER
		self.ids.tab_panel.switch_to(self.ids.item_answer)
		self.clear_answer()

	def start_recording_answer(self, *args):
		self.answerRecordingThread = threading.Thread(target=self.record_answer)
		self.answerRecordingThread.start()

	def record_answer(self, *args):
		numBars = self.level if self.level < 3 else 4
		self.answerRhythm = midi_sequencer.MidiSequencer.record_input_segment(numBars)
		time.sleep(2)
		Clock.schedule_once(self.check_answer, 1)

	def wrong_answer(self, *args):
		self.ids.tab_panel.switch_to(self.ids.item_result)
		self.ids.lbl_result_heading.text = "Oops"
		self.ids.lbl_result_body.text = "Similarity was only {}".format(self.similarity)
		if self.level == 3 and self.points >= 3:
			self.subtract_points(self.level)
		else:
			self.add_points(0)

	def correct_answer(self, *args):
		self.ids.tab_panel.switch_to(self.ids.item_result)
		self.ids.lbl_result_heading.text = "Well done!"
		self.ids.lbl_result_body.text = "Similarity: {}".format(self.similarity)
		self.add_points(self.level)


	def check_answer(self, *args):
		print("Ref has {} events, Ans has {}".format(len(self.correctRhythm), len(self.answerRhythm)))
		self.similarity = MusicLibs.get_rhythm_similarity(self.correctRhythm, self.answerRhythm)
		if self.similarity > 0.65:
			Clock.schedule_once(self.correct_answer, 0.5)
		else:
			Clock.schedule_once(self.wrong_answer, 0.5)
		Clock.schedule_once(self.start_next_question, 3)
		
		
