import mingus.core.notes as notes
from enum import Enum
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.clock import Clock
import threading
from session_manager import SessionManager
from db_manager import DbManager
from keyboard_led_sender import KeyboardLedSender


class ExerciseStates(Enum):
	NOT_STARTED = 0
	PLAYING_INTRO = 1
	PLAYING_QUESTION = 2
	WAITING_FOR_ANSWER = 3
	PLAYING_RESULT = 4

	
"""
Base class for all exercises.
Contains common functionality such as showing the intro and moving between levels.
"""
class Exercise(Screen):
	state = ExerciseStates.NOT_STARTED
	level = 1
	points = 0
	correctAnswer = ""
	testMode = False

	def __init__(self, name):
		super(Exercise,self).__init__(name=name)
		self.cancelCurrentQuestion = False
		self.playQuestionThread = threading.Thread(target=self.play_question)
		# These defaults are overridden by the exercise subclasses
		self.levelIntroText = []
		self.levelIntroText.append("This is level 1")
		self.levelIntroText.append("This is level 2")
		self.levelIntroText.append("This is level 3")

	def stop(self):
		self.cancelCurrentQuestion = True
		if self.playQuestionThread.isAlive():
			self.playQuestionThread.join()
		KeyboardLedSender.all_off()

	"""
	Whether this exercise is currently in test or practice mode.
	"""	
	def set_test_mode(self, test_mode):
		self.testMode = test_mode

	"""
	Generates the next random question and starts playing it.
	"""
	def start_next_question(self, *args):
		if self.playQuestionThread.isAlive():
			self.cancelCurrentQuestion = True
			self.playQuestionThread.join()
		self.cancelCurrentQuestion = False
		self.generate_random_question()

		self.state = ExerciseStates.PLAYING_QUESTION
		self.ids.tab_panel.switch_to(self.ids.item_question)
		self.playQuestionThread = threading.Thread(target=self.play_question)
		self.playQuestionThread.start()

	"""
	Called when the "Next Level" button is pressed on the front panel.
	Moves to the next level and re-start with a new question at the new level.
	"""
	def level_up(self, *args):
		if self.level >= 3:
			return
		self.level += 1
		self.ids.lbl_level.text = str(self.level)
		self.play_level_intro()

	"""
	Called when the "Previous Level" button is pressed on the front panel.
	Moves to the previous level and re-start with a new question at the new level.
	"""
	def level_down(self, *args):
		if self.level <= 1:
			return
		self.level -= 1
		self.ids.lbl_level.text = str(self.level)
		self.play_level_intro()

	"""
	Overridden by the individual exercise subclasses.
	This is what actually checks the answer for correctness.
	"""
	def check_answer(self, answer):
		pass

	def next_level(self):
		if self.level < 3:
			self.level += 1

	def prev_level(self):
		if self.level > 1:
			self.level -= 1

	"""
	Overridden by the individual exercise subclasses.
	"""
	def generate_random_question(self):
		pass

	"""
	Show the instructions for the exercise category.
	"""
	def play_intro(self):
		self.state = ExerciseStates.PLAYING_INTRO
		self.ids.tab_panel.switch_to(self.ids.item_instructions)

	"""
	Shows the notes about the current level of the exercise.
	This informs the user about the unique challenges of each level.
	"""
	def play_level_intro(self, *args):
		self.cancelCurrentQuestion = True
		self.state = ExerciseStates.PLAYING_INTRO
		self.ids.tab_panel.switch_to(self.ids.item_level)
		self.ids.lbl_level_heading.text = "Level {}".format(self.level)
		self.ids.lbl_level_description.text = self.levelIntroText[self.level - 1]
		if self.playQuestionThread.isAlive():
			self.playQuestionThread.join()
		self.cancelCurrentQuestion = False

	"""
	Overridden by the individual exercise subclasses.
	Show the current question to the user and wait for an answer.
	"""
	def play_question(self):
		pass

	"""
	Called when a correct answer is given.
	Increments the answer and record the attempt in the database.
	"""
	def add_points(self, pointsToAdd):
		self.points += pointsToAdd
		self.ids.lbl_points.text = str(self.points)
		DbManager.insert_results(SessionManager.get_user_id(), self.exerciseName, self.level, pointsToAdd, 1 if self.testMode else 0)

	"""
	Usually called when a wrong answer is given 
	"""
	def subtract_points(self, pointsToSubtract):
		self.points -= pointsToSubtract
		self.ids.lbl_points.text = str(self.points)
		DbManager.insert_results(SessionManager.get_user_id(), self.exerciseName, self.level, -pointsToSubtract, 1 if self.testMode else 0)

	def reset_points(self):
		self.points = 0
		self.ids.lbl_points.text = str(self.points)

	def ask_for_answer(self, *args):
		self.state = ExerciseStates.WAITING_FOR_ANSWER
		self.ids.tab_panel.switch_to(self.ids.item_answer)
		self.clear_answer()

	def play_result(self):
		self.state = ExerciseStates.PLAYING_RESULT

	"""
	Overridden by individual exercise subclasses.
	"""
	def clear_answer(self):
		pass



