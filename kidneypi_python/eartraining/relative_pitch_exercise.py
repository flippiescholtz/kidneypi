from exercise import Exercise
import random
import mingus.core.intervals as intervals
import time
from mingus.containers import Note
import midi_sequencer
import threading
from kivy.clock import Clock
from kivy.uix.popup import Popup
from kivy.uix.label import Label
from kivy.logger import Logger
from kivy.lang import Builder
from keyboard_led_sender import KeyboardLedSender

"""
Implementation of the Relative Pitch Exercise (see chapter 4.4 in the report).
The user hears two notes and has to choose whether the second was higher or lower than the first.
"""
class RelativePitchExercise(Exercise):
	
	def __init__(self, name):
		
		super(RelativePitchExercise,self).__init__(name=name)
		self.exerciseName = 'relative_pitch'
		self.levelIntroText = []
		self.levelIntroText.append("In this level, only large intervals are used so the pitch difference is clearer.")
		self.levelIntroText.append("In this level, smaller intervals of a third are added to make things more interesting!")
		self.levelIntroText.append("In this level, the smallest interval possible (minor second) is added to the mix.")

		# Defines which intervals can be played in each level.
		# The more advanced the level, the smaller the intervals asked.
		self.intervalsByLevel = []
		self.intervalsByLevel.append(["4", "5", "6", "7"]) # level 1
		self.intervalsByLevel.append(["3", "b3", "4", "5", "b5", "6", "b6","7","b7"]) # level 2
		self.intervalsByLevel.append(["1", "2", "b2", "3", "b3", "4", "5", "b5", "6", "b6","7","b7"]) # level 3

	def generate_random_question(self):
		self.referenceNote = self.generate_reference_note()
		self.intervalNote = self.generate_interval_note()
		

	"""
	Plays the two notes one second apart.
	"""
	def play_question(self):
		midi_sequencer.MidiSequencer.play_single_note(int(self.referenceNote), 2)
		time.sleep(1)
		midi_sequencer.MidiSequencer.play_single_note(int(self.intervalNote), 2)
		time.sleep(1)
		Clock.schedule_once(self.ask_for_answer, 0)

	"""
	Chooses the first note.
	For level 1, it's always middle C.
	In level 2 and 3, any note can be used.
	"""
	def generate_reference_note(self):
		# for level 1, always start on middle C:
		if self.level == 1:
			return Note('C')

		# for other levels, we use a random starting note:
		note = Note()
		note.from_int(48 + random.randint(0,12))
		return note

	"""
	Chooses the second note from a random interval chosen from this level's 
	possible intervals.
	"""
	def generate_interval_note(self):
		interval = random.choice(self.intervalsByLevel[self.level-1])
		isUp = random.choice([True, False])

		if interval == '8':
			# for octaves, we have to transpose manually:
			octave = self.referenceNote.octave + 1 if isUp else self.referenceNote.octave - 1
			note = Note(self.referenceNote.name, octave)
		else:
			note = Note(self.referenceNote.name, self.referenceNote.octave)
			note.transpose(interval, up=isUp)
		return note

	def wrong_answer(self, *args):
		self.ids.tab_panel.switch_to(self.ids.item_result)
		self.ids.lbl_result_heading.text = "Oops"
		self.ids.lbl_result_body.text = "That answer is not correct. The correct answer was '" + self.correctAnswer + "'. Let's try again."
		if self.level == 3 and self.points >= 3:
			self.subtract_points(self.level)
		else:
			self.add_points(0)

	def correct_answer(self, *args):
		self.ids.tab_panel.switch_to(self.ids.item_result)
		self.ids.lbl_result_heading.text = "Well done!"
		self.ids.lbl_result_body.text = "You are correct. Let's try another one."
		self.add_points(self.level)

		
	"""
	Called when the user presses one of the 'higher' or 'lower' numbers.
	Checks the answer on a separate threads.
	"""
	def answer_chosen(self, answer):
		checkAnswerThread = threading.Thread(target=self.check_answer, args=(answer,))
		checkAnswerThread.start()


	"""
	Checks whether the user has judged the direction of the interval correctly,
	and displays the appropriate feedback screen.
	"""
	def check_answer(self, answer):	
		self.correctAnswer = 'higher' if int(self.intervalNote) > int(self.referenceNote) else 'lower'
		if answer == 'higher' and int(self.intervalNote) > int(self.referenceNote):
			Clock.schedule_once(self.correct_answer, 0.5)
		elif answer == 'lower' and int(self.intervalNote) < int(self.referenceNote):
			Clock.schedule_once(self.correct_answer, 0.5)
		else:	
			Clock.schedule_once(self.wrong_answer, 0.5)
		Clock.schedule_once(self.start_next_question, 3)
		
		
