import mingus.core.notes as notes
from enum import Enum
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.clock import Clock
import threading
from session_manager import SessionManager
from db_manager import DbManager

"""
This class manages the view of the exercise results.

"""
class ResultsScreen(Screen):                   
	def update(self):
		print("Updating results...")
		userId = SessionManager.get_user_id()
		print("userId from session: {}".format(userId))
		self.ids.lbl_results_username.text = SessionManager.get_user_name()
		print("points coming back: {}".format(DbManager.get_total_points_for_user(userId, True)))
		self.ids.lbl_overview_points_test.text = str(DbManager.get_total_points_for_user(userId, True)) + " points"
		self.ids.lbl_overview_attempts_test.text = str(DbManager.get_total_attempts_for_user(userId, True)) + " attempts"
		self.ids.lbl_overview_points_practice.text = str(DbManager.get_total_points_for_user(userId, False)) + " points"
		print("practice points coming back: {}".format(DbManager.get_total_points_for_user(userId, False)))
		self.ids.lbl_overview_attempts_practice.text = str(DbManager.get_total_attempts_for_user(userId, False)) + " attempts"
	
#########################################
		self.ids.lbl_relative_pitch_total_points.text = str(DbManager.get_points_for_exercise(userId, 'relative_pitch', True)) + " points from " + str(DbManager.get_attempts_for_exercise(userId, 'relative_pitch', True)) + " test attempts"
		
		attempts =  DbManager.get_total_attempts_for_level(userId, 'relative_pitch', 1, True) 
		correct = DbManager.get_correct_attempts_for_level(userId, 'relative_pitch', 1, True)
		perc = str((float(correct) / float(attempts)) * 100) + ' %' if attempts > 0 else 'N/A'
		self.ids.lbl_relative_pitch_attempts_lvl_1.text =  str(correct) + " / " + str(attempts) + " attempts correct."
		self.ids.lbl_relative_pitch_percentage_lvl_1.text = perc

		attempts =  DbManager.get_total_attempts_for_level(userId, 'relative_pitch', 2, True) 
		correct = DbManager.get_correct_attempts_for_level(userId, 'relative_pitch', 2, True)
		perc = str((float(correct) / float(attempts)) * 100) + ' %' if attempts > 0 else 'N/A'
		self.ids.lbl_relative_pitch_attempts_lvl_2.text =  str(correct) + " / " + str(attempts) + " attempts correct."
		self.ids.lbl_relative_pitch_percentage_lvl_2.text = perc

		attempts =  DbManager.get_total_attempts_for_level(userId, 'relative_pitch', 3, True) 
		correct = DbManager.get_correct_attempts_for_level(userId, 'relative_pitch', 3, True)
		perc = str((float(correct) / float(attempts)) * 100) + ' %' if attempts > 0 else 'N/A'
		self.ids.lbl_relative_pitch_attempts_lvl_3.text =  str(correct) + " / " + str(attempts) + " attempts correct."
		self.ids.lbl_relative_pitch_percentage_lvl_3.text = perc

##############################3
		self.ids.lbl_interval_total_points.text = str(DbManager.get_points_for_exercise(userId, 'interval', True)) + " points from " + str(DbManager.get_attempts_for_exercise(userId, 'interval', True)) + " test attempts"

		attempts =  DbManager.get_total_attempts_for_level(userId, 'interval', 1, True) 
		correct = DbManager.get_correct_attempts_for_level(userId, 'interval', 1, True)
		perc = str((float(correct) / float(attempts)) * 100) + ' %' if attempts > 0 else 'N/A'
		self.ids.lbl_interval_attempts_lvl_1.text =  str(correct) + " / " + str(attempts) + " attempts correct."
		self.ids.lbl_interval_percentage_lvl_1.text = perc

		attempts =  DbManager.get_total_attempts_for_level(userId, 'interval', 2, True) 
		correct = DbManager.get_correct_attempts_for_level(userId, 'interval', 2, True)
		perc = str((float(correct) / float(attempts)) * 100) + ' %' if attempts > 0 else 'N/A'
		self.ids.lbl_interval_attempts_lvl_2.text =  str(correct) + " / " + str(attempts) + " attempts correct."
		self.ids.lbl_interval_percentage_lvl_2.text = perc

		attempts =  DbManager.get_total_attempts_for_level(userId, 'interval', 3, True) 
		correct = DbManager.get_correct_attempts_for_level(userId, 'interval', 3, True)
		perc = str((float(correct) / float(attempts)) * 100) + ' %' if attempts > 0 else 'N/A'
		self.ids.lbl_interval_attempts_lvl_3.text =  str(correct) + " / " + str(attempts) + " attempts correct."
		self.ids.lbl_interval_percentage_lvl_3.text = perc

################################################################
		self.ids.lbl_chord_matching_total_points.text = str(DbManager.get_points_for_exercise(userId, 'chord_matching', True)) + " points from " + str(DbManager.get_attempts_for_exercise(userId, 'chord_matching', True)) + " test attempts"

		attempts =  DbManager.get_total_attempts_for_level(userId, 'chord_matching', 1, True) 
		correct = DbManager.get_correct_attempts_for_level(userId, 'chord_matching', 1, True)
		perc = str((float(correct) / float(attempts)) * 100) + ' %' if attempts > 0 else 'N/A'
		self.ids.lbl_chord_matching_attempts_lvl_1.text =  str(correct) + " / " + str(attempts) + " attempts correct."
		self.ids.lbl_chord_matching_percentage_lvl_1.text = perc

		attempts =  DbManager.get_total_attempts_for_level(userId, 'chord_matching', 2, True) 
		correct = DbManager.get_correct_attempts_for_level(userId, 'chord_matching', 2, True)
		perc = str((float(correct) / float(attempts)) * 100) + ' %' if attempts > 0 else 'N/A'
		self.ids.lbl_chord_matching_attempts_lvl_2.text =  str(correct) + " / " + str(attempts) + " attempts correct."
		self.ids.lbl_chord_matching_percentage_lvl_2.text = perc

		attempts =  DbManager.get_total_attempts_for_level(userId, 'chord_matching', 3, True) 
		correct = DbManager.get_correct_attempts_for_level(userId, 'chord_matching', 3, True)
		perc = str((float(correct) / float(attempts)) * 100) + ' %' if attempts > 0 else 'N/A'
		self.ids.lbl_chord_matching_attempts_lvl_3.text =  str(correct) + " / " + str(attempts) + " attempts correct."
		self.ids.lbl_chord_matching_percentage_lvl_3.text = perc

################################		
		self.ids.lbl_rhythm_total_points.text = str(DbManager.get_points_for_exercise(userId, 'rhythm', True)) + " points from " + str(DbManager.get_attempts_for_exercise(userId, 'rhythm', True)) + " test attempts"
		
		attempts =  DbManager.get_total_attempts_for_level(userId, 'rhythm', 1, True) 
		correct = DbManager.get_correct_attempts_for_level(userId, 'rhythm', 1, True)
		perc = str((float(correct) / float(attempts)) * 100) + ' %' if attempts > 0 else 'N/A'
		self.ids.lbl_rhythm_attempts_lvl_1.text =  str(correct) + " / " + str(attempts) + " attempts correct."
		self.ids.lbl_rhythm_percentage_lvl_1.text = perc

		attempts =  DbManager.get_total_attempts_for_level(userId, 'rhythm', 2, True) 
		correct = DbManager.get_correct_attempts_for_level(userId, 'rhythm', 2, True)
		perc = str((float(correct) / float(attempts)) * 100) + ' %' if attempts > 0 else 'N/A'
		self.ids.lbl_rhythm_attempts_lvl_2.text =  str(correct) + " / " + str(attempts) + " attempts correct."
		self.ids.lbl_rhythm_percentage_lvl_2.text = perc

		attempts =  DbManager.get_total_attempts_for_level(userId, 'rhythm', 3, True) 
		correct = DbManager.get_correct_attempts_for_level(userId, 'rhythm', 3, True)
		perc = str((float(correct) / float(attempts)) * 100) + ' %' if attempts > 0 else 'N/A'
		self.ids.lbl_rhythm_attempts_lvl_3.text =  str(correct) + " / " + str(attempts) + " attempts correct."
		self.ids.lbl_rhythm_percentage_lvl_3.text = perc
