from kivy.uix.screenmanager import ScreenManager, Screen
from relative_pitch_exercise import RelativePitchExercise
from rhythm_exercise import RhythmExercise
from interval_exercise import IntervalExercise
from chord_matching_exercise import ChordMatchingExercise
from kivy.lang import Builder
from training_manager_screen import TrainingManagerScreen
from kivy.clock import Clock
from panel_led_sender import PanelLedSender
from results_screen import ResultsScreen

class TrainingManager:

	@classmethod
	def initialize(self, screenManager):

		self.screenManager = screenManager
		
		self.currentExerciseIndex = 0
		self.active = False
		self.testMode = False

		Builder.load_file('eartraining/layouts/interval_exercise.kv')
		self.intervalExercise = IntervalExercise(name='interval_exercise')
		Builder.load_file('eartraining/layouts/relative_pitch_exercise.kv')
		self.relativePitchExercise = RelativePitchExercise(name='relative_pitch_exercise')
		Builder.load_file('eartraining/layouts/rhythm_exercise.kv')
		self.rhythmExercise = RhythmExercise(name='rhythm_exercise')
		Builder.load_file('eartraining/layouts/chord_matching_exercise.kv')
		self.chordMatchingExercise = ChordMatchingExercise(name='chord_matching_exercise')
		Builder.load_file('eartraining/layouts/results.kv')
		self.resultsScreen = ResultsScreen(name='results')

		self.screenManager.add_widget(self.relativePitchExercise)
		self.screenManager.add_widget(self.intervalExercise)
		self.screenManager.add_widget(self.rhythmExercise)
		self.screenManager.add_widget(self.chordMatchingExercise)
		self.screenManager.add_widget(self.resultsScreen)
		
		self.exercises = [self.relativePitchExercise, self.intervalExercise, self.rhythmExercise, self.chordMatchingExercise]
		self.currentExercise = self.exercises[self.currentExerciseIndex]

	@classmethod
	def start_button_pressed(self, *args):
		if self.active:
			self.testMode = not self.testMode
		else:
			self.testMode = False
		self.start()

	@classmethod
	def start(self, *args):
		self.active = True
		self.currentExercise = self.exercises[self.currentExerciseIndex]
		self.screenManager.current = self.currentExercise.name
		self.currentExercise.reset_points()
		self.currentExercise.set_test_mode(self.testMode)
		self.update_status_led()
		self.currentExercise.play_intro()

	@classmethod
	def next_category(self):
		if self.currentExerciseIndex < len(self.exercises) - 1:
			self.stop()
			self.currentExerciseIndex += 1
			Clock.schedule_once(self.start)

	@classmethod
	def deactivate(self):
		self.active = False
		self.update_status_led()

	@classmethod
	def stop(self):
		self.currentExercise.stop()

	@classmethod
	def show_results(self):
		self.stop()
		self.screenManager.current = 'results'
		self.resultsScreen.update()
		

	@classmethod
	def update_status_led(self):
		PanelLedSender.set_led(PanelLedSender.START_RED, self.active and self.testMode)
		PanelLedSender.set_led(PanelLedSender.START_GREEN, self.active and not self.testMode) 

	@classmethod
	def prev_category(self):
		if self.currentExerciseIndex > 0:
			self.stop()
			self.currentExerciseIndex -= 1
			Clock.schedule_once(self.start)

	@classmethod
	def level_up(self):
		self.stop()
		Clock.schedule_once(self.currentExercise.level_up)

	@classmethod
	def level_down(self):
		self.stop()
		Clock.schedule_once(self.currentExercise.level_down)

