from kivy.app import App
from kivy.lang import Builder
from kivy.uix.screenmanager import Screen, ScreenManager


class TrainingManagerScreen(Screen):
	def get_screen_manager(self):
		return self.ids.training_screen_manager
