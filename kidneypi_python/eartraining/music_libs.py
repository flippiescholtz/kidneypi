import mingus.core.notes as notes
from mingus.containers import Note
import mingus.core.intervals as intervals
import midi
from kivy.logger import Logger
import math

"""
This class contains various utility methods for checking musical answers, etc.
"""
class MusicLibs:

	# Each musical note has multiple possible names. 'D-sharp' is also 'E-flat'.
	# These equivalent note names are known as 'enharmonic notes'.
	# This list defines all these alternative names for each note.
	# We need this because we want to accept any of the equivalent names for a note when a student answers.
	enharmonicNotes = {}
	# sharps:
	enharmonicNotes['C'] = ['C', 'B#', 'Dbb']
	enharmonicNotes['C#'] = ['C#', 'Db', 'B##']
	enharmonicNotes['D'] = ['D','C##', 'Ebb']
	enharmonicNotes['D#'] = ['D#', 'Eb', 'Fbb']
	enharmonicNotes['E'] = ['E','Fb', 'D##']
	enharmonicNotes['E#'] = ['F', 'Gbb']
	enharmonicNotes['F'] = ['F','E#', 'Gbb']
	enharmonicNotes['F#'] = ['F#','Gb', 'E##']
	enharmonicNotes['G'] = ['G','F##', 'Abb']
	enharmonicNotes['G#'] = ['G#','Ab']
	enharmonicNotes['A'] = ['A','G##', 'Bbb']
	enharmonicNotes['A#'] = ['A#','Bb', 'Cbb']
	enharmonicNotes['B'] = ['B','A##', 'Cb']
	enharmonicNotes['B#'] = ['C', 'Dbb']

	# flats:
	enharmonicNotes['Db'] = ['Db','C#', 'B##']
	enharmonicNotes['Eb'] = ['Eb','D#', 'Fbb']
	enharmonicNotes['Gb'] = ['Gb','F#', 'E##']
	enharmonicNotes['Ab'] = ['Ab','G#']
	enharmonicNotes['Bb'] = ['Bb','A#', 'Cbb']
	enharmonicNotes['Fb'] = ['E', 'D##']
	enharmonicNotes['Cb'] = ['B', 'A##']

	# double sharps:
	enharmonicNotes['C##'] = ['D','Ebb']
	enharmonicNotes['D##'] = ['E', 'Fb']
	enharmonicNotes['E##'] = ['F#','Gb']
	enharmonicNotes['F##'] = ['G','Abb']
	enharmonicNotes['G##'] = ['A','Bbb']
	enharmonicNotes['A##'] = ['B', 'Cb']
	enharmonicNotes['B##'] = ['C#', 'Db']

	# double flats:
	enharmonicNotes['Cbb'] = ['Bb','A#']
	enharmonicNotes['Dbb'] = ['C', 'B#']
	enharmonicNotes['Ebb'] = ['D','C##']
	enharmonicNotes['Fbb'] = ['Eb','D#']
	enharmonicNotes['Gbb'] = ['F','E#']
	enharmonicNotes['Abb'] = ['G', 'F##']
	enharmonicNotes['Bbb'] = ['A', 'G##']

	#################################################

	"""
	Returns an interval name that is the most valid for the two given notes.
	"""
	@classmethod
	def get_most_correct_interval(self, firstNote, secondNote):
		# if the notes are an octave apart, 'perfect octave' is the most correct:
		if firstNote.name == secondNote.name and abs(firstNote.octave - secondNote.octave) == 1:
			# notes are an octave apart:
			return "perfect octave"
		return intervals.determine(firstNote.name, secondNote.name)

	"""
	Checks whether the given interval name is valid for the two given notes.
	"""
	@classmethod
	def is_interval_valid(self, firstNote, secondNote, intervalDescription):
		intervalDescription = intervalDescription.lower().strip()

		if intervalDescription == self.get_most_correct_interval(firstNote, secondNote):
			# The user's answer agrees with the mingus library. It's correct. Nothing more to do:
			return True

		# The user did not agree with Mingus, but the answer might still be correct
		# because of enharmonic notes.
		# Loop through each enharmonic combination of the two note names, and
		# check if the selected interval is valid for any of them:
		for first in self.enharmonicNotes[firstNote.name]:
			for second in self.enharmonicNotes[secondNote.name]:
				mingusInterval = intervals.determine(first, second).lower()
				# for unisons, accept the answer if the word 'unison' is in both descriptions,
				# without regard for the tonality.
				if 'unison' in intervalDescription and 'unison' in mingusInterval:
					return True

				# mingus regards a diminished fifth as a minor fifth, but we want to accept
				# 'diminished fifth' as correct as well, hence the second part of the if statement:
				if mingusInterval == intervalDescription or (mingusInterval == 'minor fifth' and intervalDescription == 'diminished fifth'):
					return True
		# We couldn't determine this answer to be correct.
		return False

	"""
	Converts a list of musical note names into MIDI note numbers
	that can be sent to our MIDI tone generators.
	"""
	@classmethod
	def note_names_to_numbers(self, noteNames):
		# First we determine whether to start above or below middle C:
		noteNumbers = []
		if noteNames[0][0] in ["C", "D", "E", "F"]:
			# start above middle C:
			currentNote = self.next_note_above_with_name(60, noteNames[0], True)
		else:
			# start below middle C:
			currentNote = self.next_note_below_with_name(60, noteNames[0], True)
		noteNumbers.append(int(currentNote))

		for i in range(1, len(noteNames)):
			#print("next above {} with name {}:".format(int(currentNote), noteNames[i]))
			currentNote = self.next_note_above_with_name(int(currentNote), noteNames[i], False)
			#print("is {}".format(int(currentNote)))
			noteNumbers.append(int(currentNote))
		return noteNumbers


	"""
	Get the next note number above startAtNumber that has the name 'findName'.
	Used when converting musical chords to their MIDI note numbers.
	"""
	@classmethod
	def next_note_above_with_name(self, startAtNumber, findName, inclusive=False):
		n = Note()
		n.from_int(startAtNumber if inclusive else startAtNumber+1)
		while n.name not in self.enharmonicNotes[findName]:
			n.from_int(int(n)+1)

		return n

	"""
	Get the next note number below startAtNumber that has the name 'findName'.
	Used when converting musical chords to their MIDI note numbers.
	"""
	@classmethod
	def next_note_below_with_name(self, startAtNumber, findName, inclusive=False):
		n = Note()
		n.from_int(startAtNumber if inclusive else startAtNumber-1)
		while n.name not in self.enharmonicNotes[findName]:
			n.from_int(int(n)-1)

		return n

	"""
	Returns only the Note On events from a list of MIDI events.
	This is used for the rhythm repetition exercise when it compares
	the reference rhythm to the answer rhythm.
	"""
	@classmethod
	def get_note_on_events(self, events):
		noteOnEvents = []
		for e in events: 
			if isinstance(e, midi.NoteOnEvent) and e.velocity > 0:
				noteOnEvents.append(e)
		return noteOnEvents

	"""
	Returns a list of delta ticks between each of the Note On events
	in a list. Used during the calculation process when comparing rhythms
	in the rhythm exercise.
	"""
	@classmethod
	def get_ticks_between_note_on_events(self, events):
		noteOnEvents = self.get_note_on_events(events)
		if len(noteOnEvents) == 0:
			return [0]
		if len(noteOnEvents) == 1:
			return [noteOnEvents[0].tick]
		distances = []
		for i in range(1, len(noteOnEvents)):
			distances.append(noteOnEvents[i].tick - noteOnEvents[i-1].tick)
		return distances

	"""
	Finds a list of every sequential combination of n items in the input_list.
	Used during the rhythm comparison in the rhythm exercise.
	This step is part of an algorithm developed by Hsiao, Liang and Ke:
	http://www.music-ir.org/mirex/abstracts/2008/QT_show.pdf

	This ngram code was adapted from a blog post by Scott Triglia:
	http://locallyoptimal.com/blog/2013/01/20/elegant-n-gram-generation-in-python/
	"""
	@classmethod
	def find_ngrams(self, input_list, n):
		if len(input_list) < n:
			return [input_list]
  		return zip(*[input_list[i:] for i in range(n)])

  	"""
  	Compares two rhythms using the algorithm proposed by Hsiao et. al.
  	http://www.music-ir.org/mirex/abstracts/2008/QT_show.pdf

  	This method implements their algorithm in Python, apart from some modifications
  	as indicated.
  	"""
  	@classmethod
  	def get_rhythm_similarity(self, rhythm1, rhythm2):
		Logger.info("Starting with {}, {}".format(len(rhythm1), len(rhythm2)))

  		rhythm1 = self.get_ticks_between_note_on_events(rhythm1)
  		rhythm2 = self.get_ticks_between_note_on_events(rhythm2)

  		ngrams1 = self.find_ngrams(rhythm1, 4)
  		ngrams2 = self.find_ngrams(rhythm2, 4)

  		# This if statement is my addition, not part of Hsiao et. al.'s algorithm
		if len(ngrams1) == 0 and len(ngrams2) == 0:
			# Silence was asked and silence was given. They get 100% :-)
			return 1.0
		
  		numNgrams = min(len(ngrams1), len(ngrams2))
		
		# This check was not in Hsiao et. al.'s algorithm.
		if numNgrams == 0:
			# One rhythm had nothing and the other had something. Totally wrong.
			return 0		

  		stddevs = [] # standard deviations for each ngram
  		offsetFactors = [] # offset factors as defined in Hsiao et. al.'s paper.
  		ngramSimilarities = []


  		for n in range(numNgrams):
  			quotients = []
  			numElements = min(len(ngrams1[n]), len(ngrams2[n]))
  			for i in range(numElements):
  				quotients.append(float(ngrams1[n][i]) / float(ngrams2[n][i]))
  			stddevs.append(self.standard_deviation(quotients))
  			offsetFactors.append(sum(quotients)/float(len(quotients)))
  			ngramSimilarities.append(1 - (stddevs[n]/offsetFactors[n]))

  		return sum(ngramSimilarities)/float(len(ngramSimilarities))
 
 	"""	
	Calculates the standard deviation for a list of numbers.
	Code written by Josh Cohen (http://codeselfstudy.com/blogs/how-to-calculate-standard-deviation-in-python)
	"""
	@classmethod
	def standard_deviation(self, lst, population=True): 
		num_items = len(lst)
    	mean = sum(lst) / num_items
    	differences = [x - mean for x in lst]
		sq_differences = [d ** 2 for d in differences]
		ssd = sum(sq_differences)
 
		if population is True:
			variance = ssd / num_items
		else:
			variance = ssd / (num_items - 1)
		return math.sqrt(variance)



