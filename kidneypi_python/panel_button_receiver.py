import time
import math
import threading
import main_controller
from panel_led_sender import PanelLedSender
import RPi.GPIO as GPIO
import zmq

"""
This class receives ZeroMQ events from C++ relating to the panel buttons.
The panel buttons are scanned in the C++ class in 'kidneypi_c/PanelButtonScanner.cpp'.
When the system starts up, a this class waits for ZeroMQ messages in a separate thread,
and notifies the MainController class of any panel button events it receives.
"""
class PanelButtonReceiver:
	# Some constants defining all the panel buttons and their indices in the list:
	HOME = 0
	SOUND_1 = 1
	SOUND_2 = 2
	SOUND_3 = 3
	TRACK_1 = 4
	TRACK_2 = 5
	SOUND_KB_SELECT = 6
	SOUND_4 = 7
	SOUND_5 = 8
	SOUND_6 = 9
	TRACK_3 = 10
	TRACK_4 = 11
	REC = 12
	SOUND_7 = 13
	SOUND_8 = 14
	SOUND_9 = 15
	TRACK_5 = 16
	TRACK_6 = 17
	STOP = 18
	LOGOUT = 19
	SOUND_0 = 20
	ASSIGNABLE_1 = 21
	TRACK_7 = 22
	TRACK_8 = 23
	ASSIGNABLE_2 = 24
	ASSIGNABLE_3 = 25
	ASSIGNABLE_4 = 26
	ASSIGNABLE_5 = 27
	CAT_PREV = 28
	CAT_NEXT = 29
	PLAY = 30
	RW = 31
	FF = 32
	TRACK_KB_SELECT = 33
	LEVEL_PREV = 34
	LEVEL_NEXT = 35
	LOAD = 36
	SAVE = 37
	PART = 38
	ASSIGNABLE_6 = 39
	START = 40
	RESULTS = 41

	# If this is set to false, break out of the loop and stop listening.
	_listening = False
	receiverThread = None	

	"""
	Called from the MainController class when the system starts up.
	"""
	@classmethod
	def start(self):
		print("Starting panel button listener")
		
		self.receiverThread = threading.Thread(target=self.listen)
		self._listening = True
		self.receiverThread.start()

	@classmethod
	def stop(self):
		if self._listening:
			self._listening = False
			self.receiverThread.join()

	"""
	Converts a Sound Select number (0 - 9) to its corresponding
	button constant as defined at the top of this class.
	"""
	@classmethod
	def button_number_to_sound_index(self, button_number):
		if button_number == self.SOUND_0:
			return 0
		if button_number == self.SOUND_1:
			return 1
		if button_number == self.SOUND_2:
			return 2
		if button_number == self.SOUND_3:
			return 3
		if button_number == self.SOUND_4:
			return 4
		if button_number == self.SOUND_5:
			return 5
		if button_number == self.SOUND_6:
			return 6
		if button_number == self.SOUND_7:
			return 7
		if button_number == self.SOUND_8:
			return 8
		if button_number == self.SOUND_9:
			return 9
		return -1 # not a sound select button

	"""
	Converts a sequencer track number (1-8) to its corresponding
	button constant as defined at the top of this class.
	"""
	@classmethod
	def button_number_to_track_index(self, button_number):
		if button_number == self.TRACK_1:
			return 0
		if button_number == self.TRACK_2:
			return 1
		if button_number == self.TRACK_3:
			return 2
		if button_number == self.TRACK_4:
			return 3
		if button_number == self.TRACK_5:
			return 4
		if button_number == self.TRACK_6:
			return 5
		if button_number == self.TRACK_7:
			return 6
		if button_number == self.TRACK_8:
			return 7
		return -1 # not a track button


	"""
	This runs in a separate thread and listens for ZeroMQ messages from C++
	relating to panel button events.
	"""
	@classmethod
	def listen(self):
		zmqContext = zmq.Context()
		zmqSocket = zmqContext.socket(zmq.SUB) # The C++ side is the publisher, this class is the subscriber.
		zmqSocket.connect("tcp://127.0.0.1:5558") # The C++ class transmits on local TCP port 5558
		topicfilter = "10002" # Not used.
		zmqSocket.setsockopt(zmq.SUBSCRIBE, topicfilter)
		
		print "Waiting for button presses from c++"
		msg = ""
		while self._listening:
			try:
				msg = zmqSocket.recv(flags=zmq.NOBLOCK)
			except zmq.ZMQError:
				# No message has been received. Wait 0.2 seconds before checking again.
				time.sleep(0.2)
				continue
			
			# tokens[1] is the event type: 1 for press, 2 for long press
			tokens = msg.rstrip(' \t\t\n\0').split()
			
			if tokens[1] == "1":
				# Button pressed event.
				# Notify the MainController which will dispatch it to other classes that need it.
				main_controller.MainController.button_pressed(int(tokens[2]))
			elif tokens[1] == "2":
				# Button held (long press) event.
				# Notify the MainController which will dispatch it to other classes that need it.
				main_controller.MainController.button_held(int(tokens[2]))	







