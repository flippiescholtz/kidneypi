from db_manager import DbManager
from panel_led_sender import PanelLedSender

"""
This class stores details about the currently logged-in user.
When the user interface detects a login or logout event, it calls this class,
which loads the user details from the database.
If any other code needs access to the current user details (e.g. for display),
it gets it from this class. 
"""
class SessionManager:
	user = {}

	"""
	Turns the "Login" LED on if a user is logged in, or off if nobody is logged in.
	"""
	@classmethod
	def update_login_led(self):
		PanelLedSender.set_led(PanelLedSender.LOGIN, self.is_logged_in())

	"""
	Checks whether someone is currently logged in.
	"""
	@classmethod
	def is_logged_in(self):
		return bool(self.user)

	"""
	Log in the user with the given ID as stored in the database.
	"""
	@classmethod
	def login(self, user_id):
		self.user = DbManager.get_user(user_id)
		self.update_login_led()

	"""
	Called when the 'Logout' button is pressed on the front panel.
	"""
	@classmethod
	def logout(self):
		self.user = {}
		self.update_login_led()

	"""
	Returns the currently logged-in user.
	"""
	@classmethod
	def get_user(self):
		return self.user

	@classmethod
	def get_user_id(self):
		return self.user['id']

	@classmethod
	def get_user_name(self):
		return self.user['user_name'] 
