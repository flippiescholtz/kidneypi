import midi
import Queue
import threading
from panel_led_sender import PanelLedSender
from panel_button_receiver import PanelButtonReceiver
import gui_manager
import time
from threading import RLock
import traceback
from copy import deepcopy
import midi.sequencer as sequencer
from kivy.logger import Logger
from kivy.clock import Clock
from pprint import pprint

"""
This class handles all functionality relating to the multi-track MIDI sequencer.
Its main function is to record to one of the 8 song tracks,
but it is also used to record material for the Rhythm Memorization exercise.

In future versions, this class could be refactored into multiple smaller classes
to make the code more readable.
"""
class MidiSequencer:
	RESOLUTION = 120   # ticks per beat
	beatsPerMeasure = 4
	tempo = 120 # beats per minute
	TRACK_CHANNELS = [4, 5, 6, 7, 8, 12, 13, 9] # MIDI channels used by each track
	TRACK_VOLUMES = [100, 100, 100, 100, 100, 100, 100, 100] # MIDI channels used by each track
	TRACK_MUTES = [False, False, False, False, False, False, False, False]
	TRACK_SOLOS = [False, False, False, False, False, False, False, False]
	TRACK_PATCHNAMES = ["", "", "", "" ,"", "", "", ""] # instrument names like 'Piano, Flute...' for each track.
	busyRecording = False
	busyPlaying = False
	tick = 0
	selectedKeyboard = 0 # 0 for internal keyboard, 1 for external MIDI-connected keyboard.

	nonTrackMode = False # If true, events are recorded to a separate list for returning to a caller, and not recorded to the multitrack system.
	nonTrackEvents = [] # MIDI sequence requested by an external caller...not recorded to multitrack sequencer.
	metronomeEvents = [] # if we are using a metronome, playback these events.
	stopAfterTick = -1 # if positive, we must stop recording after this tick
	patternName = 'Untitled'
	seqLock = threading.RLock()
	seqStarted = False

	"""
	Called from the MainController when the system starts up.
	Initializes the various ALSA MIDI ports and other sequencer variables.
	"""
	@classmethod
	def initialize(self):
		"""
		If nonTrackMode is True, the sequencer is recording material for the
		Rhythm exercise rather than recording to one of the song tracks.
		"""
		self.nonTrackMode = False
		self.recordingThread = threading.Thread(target=self.record)
		self.playbackThread = threading.Thread(target=self.playback)
		self.seq = sequencer.SequencerDuplex(sequencer_name='kidneypi_sequencer',sequencer_resolution=self.RESOLUTION)
		self.seq.set_nonblock(True)

		"""
		Connect to the device's MIDI Out port (i.e. ttymidi's MIDI in port).
		During playback, notes will be sent to this port and hence be heard
		on the external MIDI keyboard's tone generators.
		"""
		hardware = sequencer.SequencerHardware()
		outputClient = hardware.get_client("ttymidi")
		outputPort = outputClient.get_port("MIDI in")
		self.seq.subscribe_write_port(outputClient.client, outputPort.port)		

		"""
		The system is currently starting up, so we play a 'demo' scale,
		sounding all the notes on the keyboard rapidly after one another.
		This lets the user know that the MIDI connection is working.
		"""
		self.play_demo_scale()	

		"""
		Connect to the two ALSA MIDI input ports that we will record notes from.
		IntKB for the internal keyboard, and ttymidi's MIDI out port 
		for the externally connected keyboard. 
		"""
		self.inputClientInternal = hardware.get_client("IntKB")
		self.inputPortInternal = self.inputClientInternal.get_port("IntKBOut")
		self.seq.subscribe_read_port(self.inputClientInternal.client, self.inputPortInternal.port)

		self.inputClientExternal = hardware.get_client("ttymidi")
		self.inputPortExternal = self.inputClientExternal.get_port("MIDI out")
		self.seq.subscribe_read_port(self.inputClientExternal.client, self.inputPortExternal.port)

		# Make sure the tempo is updated.
		self.seq.change_tempo(self.tempo)		

		# Inittialize a new recording.
		self.new_recording()

	
	"""
	Called at startup and whenever the user 'long-presses' the Record button.
	This erases the currently stored recording and initializes a new one.
	Recording does not actually start until the user presses 'Record' again.
	"""	
	@classmethod
	def new_recording(self):
		"""
		If stopAfterTick is positive, the system will only record until 
		the specified tick is reached. This is used when recording rhythm material
		for the 'Rhythm memorization' ear training exercise.
		"""
		self.stopAfterTick = -1 
		self.nonTrackMode = False # If True, we are recording material for the rhythm exercise and not a song track.
		self.nonTrackEvents = []
		self.pattern = midi.Pattern(tick_relative=False, resolution=self.RESOLUTION)
		self.patternName = 'Untitled'
		for t in range(8):
			self.pattern.append(midi.Track(tick_relative=False))
		self.set_tempo(120)
		self.armedTrackNumbers = [0,-1] # To which tracks are the internal and external keyboards recording, respectively.
		self.busyRecording = False
		self.tick = 0
		
		self.update_track_leds()
		Clock.schedule_once(gui_manager.GUIManager.sequencerScreen.update_gui)


	"""
	Called when the tempo changes. Computes the new tick durations
	from the new tempo and the sequencer's resolution.
	There is currently no way to set the tempo from the user interface.
	That could be implemented in future versions.
	"""
	@classmethod
	def set_tempo(self, tempo):
		self.tempo = tempo
		beatsPerSecond = float(self.tempo) / float(60)
		secsPerBeat = float(1) / beatsPerSecond
		self.secsPerTick = secsPerBeat / float(self.RESOLUTION)
		self.ticksPerSec = float(1) / self.secsPerTick
		self.ticksPerBeat = secsPerBeat * self.ticksPerSec
		tempoEvent = midi.SetTempoEvent(tick=self.tick)
		tempoEvent.set_bpm(self.tempo)
		self.pattern[0].append(tempoEvent)

	"""
	This method starts the recording. When the user now plays on the keyboard(s),
	the notes will be recorded to the armed tracks.
	"""
	@classmethod
	def start_recording(self):
		if self.busyRecording:
			# We are already recording, so don't try to start again.
			return
		self.seq.stop_sequencer() # Make sure the ALSA sequencer is stopped before we start again.
		self.seqStarted = False
		self.busyRecording = True

		"""
		While recording, we still want to hear previously recorded material play back,
		so we need to start the playback thread as well.
		"""
		self.recordingThread = threading.Thread(target=self.record)
		self.playbackThread = threading.Thread(target=self.playback)
		
		self.playbackThread.start()
		self.recordingThread.start()

		if not self.nonTrackMode:
			"""
			We are recording to the multitrack song and not the rhythm exercise,
			so do all necessary things to start a multitrack recording.

			First, we fetch the currently selected instrument numbers for each keyboard
			from the home screen:
			"""
			patchKbInt = gui_manager.GUIManager.homeScreen.get_upper1_patchnumber(0)
			patchKbExt = gui_manager.GUIManager.homeScreen.get_upper1_patchnumber(1)
			
 			# insert a program change event with the currently selected Upper1 sound for each keyboard:
 			if(patchKbInt >= 0):
				channel = self.TRACK_CHANNELS[self.armedTrackNumbers[0]]
				self.pattern[self.armedTrackNumbers[0]].append(midi.ProgramChangeEvent(tick=0,channel=channel, data=[patchKbInt]))
				self.TRACK_PATCHNAMES[self.armedTrackNumbers[0]] = gui_manager.GUIManager.homeScreen.get_upper1_patchname(0)

			if(patchKbExt >= 0):
				channel = self.TRACK_CHANNELS[self.armedTrackNumbers[1]]
				self.pattern[self.armedTrackNumbers[1]].append(midi.ProgramChangeEvent(tick=0,channel=channel, data=[patchKbExt]))
				self.TRACK_PATCHNAMES[self.armedTrackNumbers[1]] = gui_manager.GUIManager.homeScreen.get_upper1_patchname(1)
		
			"""
			The recording is now running. Show the sequencer screen and the Record LED:
			"""
			gui_manager.GUIManager.show_screen('sequencer')
			PanelLedSender.led_on(PanelLedSender.REC_RED)
		
		
	"""
	This method starts playback. It is called when the user presses
	the 'Play' button. It is NOT called before recording starts.

	If a list of 'events' is passed in, those events will be played in tick order.
	Otherwise, the events stored in the current song will be played.

	If 'numBars' is positive, the playback will only last for the specified number
	of bars (in this version, a bar can only be 4 beats long).
	"""	
	@classmethod
	def start_playback(self, events=[], numBars=-1):
		if self.busyRecording or self.busyPlaying:
			# Sequencer is already running, so we can't start playback now.
			return
		self.seq.stop_sequencer() # Make sure the ALSA sequencer has stopped before we start.
		self.seqStarted = False
		if len(events) > 0:
			# The caller wants to play a specific set of events
			# rather than what's recorded on the multitrack system.
			self.nonTrackMode = True
			self.nonTrackEvents = events
			if numBars > 0:
				"""
				For the rhythm memorization exercise, a metronome click plays
				while recording happens. Generate those events:
				"""
				self.metronomeEvents = self.generate_metronome_events(numBars, 1)

		self.busyPlaying = True
		# MIDI events are sent to the MIDI ports on a separate thread.
		self.playbackThread = threading.Thread(target=self.playback)
		self.playbackThread.start()
		if not self.nonTrackMode:
			gui_manager.GUIManager.show_screen('sequencer')


	"""
	Called when the user presses the 'stop' button on the front panel.
	Stops both playback and recording threads.
	"""
	@classmethod
	def stop_sequencer(self, *args):
		self.seq.drain()
		self.seq.drop_output()
		self.seq.stop_sequencer()
		self.seqStarted = False
		self.busyRecording = False
		self.busyPlaying = False
		self.nonTrackEvents = []

		if self.recordingThread.isAlive():
			self.recordingThread.join()		

		if self.playbackThread.isAlive():
			self.playbackThread.join()
		
		"""
		Make sure there are no 'stuck' notes lingering
		by sending Note Off messages to all notes.
		"""
		self.all_notes_off()

		PanelLedSender.led_off(PanelLedSender.REC_RED)
		PanelLedSender.led_off(PanelLedSender.PLAY_GREEN)
		self.tick = 0
		self.update_track_leds()
		if not self.nonTrackMode:	
			gui_manager.GUIManager.show_screen('sequencer')
		self.nonTrackMode = False

		
	@classmethod
	def all_notes_off(self):
		self.seqLock.acquire()
		for t in range(8):
			ev = midi.ControlChangeEvent()
			ev.data = [123, 0] # 123 is the 'All Note Off' controller in the MIDI standard.
			ev.channel = self.TRACK_CHANNELS[t]
			buf = self.seq.event_write(ev, True, False, False)
		self.seqLock.release()


	"""
	This is called when a MIDI event is received on one of the keyboards
	while recording. It places the event in the correct list based on the
	currently armed song track or rhythm exercise.

	If 'keyboard' is 0, the event came from the internal keyboard.
	If 'keyboard' is 1, the event came from an external MIDI-connected keyboard.
	"""
	@classmethod
	def record_event(self, event, keyboard=0):

		if self.nonTrackMode:
			# We are recording a short single-track segment for a caller (the rhythm exersize).
			# Put the events in a separate list and not a song track:
			event.channel = 1
			self.nonTrackEvents.append(event)
			return
		
		# This event should be recorded to the multitrack system onto one of the song tracks:
		if self.armedTrackNumbers[keyboard] < 0:
			# No track is armed for this keyboard. Nothing to do.
			return
		# Get the track number we must record to (i.e. which track is armed on this keyboard?)
		trackIndex = self.armedTrackNumbers[keyboard]

		"""
		If the 'drums' sound is selected, all notes are recorded to channel 10,
		because the General MIDI standard defines channel 10 as being for percussion.
		"""
		if(gui_manager.GUIManager.homeScreen.get_upper1_patchnumber(keyboard) == -1):
			event.channel = 9 # zero-based, so MIDI channel 10.
		else:
			# Not drums, so place it on the track that is armed.
			event.channel = self.TRACK_CHANNELS[self.armedTrackNumbers[keyboard]]
		track = self.pattern[trackIndex]
		track.append(event)

	"""
	This runs in a separate thread and listens for MIDI messages from the two keyboards.
	When a message is received, it gets sent to the appropriate method for processing and storage.
	"""
	@classmethod
	def record(self):
		self.seq.drop_output()
		self.seq.change_tempo(self.tempo)
		
		while(not self.seqStarted):
			# wait for the sequencer to start before we start listening.
			pass

		"""
		Keep listening for events until the recorder is stopped
		or the 'stopAfterTick' value is reached.
		"""
		while self.busyRecording and (self.stopAfterTick < 0 or self.seq.queue_get_tick_time() <= self.stopAfterTick + 5): # a buffer of 5 ticks
			event = self.seq.event_read()
			
			if event is not None:
				keyboard = 1 if event.source.client == self.inputClientExternal.client else 0
				self.record_event(event, keyboard)
			else:
				"""
				Check every 50 ms.
				This doesn't have to be accurate because ALSA records the MIDI timestamps
				for us at a lower level. 
				This is the beauty of using ALSA over trying to do MIDI timing in Python.
				"""
				time.sleep(0.05)


	"""
	During recording and playback, this runs on a separate thread and sends existing
	song data to the MIDI output ports for playback.
	"""
	@classmethod
	def playback(self):
		if self.nonTrackMode:
			# We're recording / playing a short single-track segment for a caller. Just play the metronome events:
			eventsToPlay = self.metronomeEvents
			for ev in eventsToPlay:
				print("click at {}".format(ev.tick))
			print("Stop after tick {}".format(self.stopAfterTick))
			if len(self.nonTrackEvents) > 0:
				eventsToPlay.extend(self.nonTrackEvents)
			self.play_events(eventsToPlay)
			return

		# We're playing from the multitrack system. Send the events stored in the tracks.
		self.pattern.make_ticks_abs()
		events = []

		for t in range(8):
			track = self.pattern[t]
			if self.busyRecording and (t == self.armedTrackNumbers[0] or t == self.armedTrackNumbers[1]):
				# We are recording and this is an armed track. Don't play its previous contents.
				continue
			for event in track:
        		events.append(event)
		self.play_events(events)	

	"""
	Returns the number of the last tick in the current recording.
	This is used when saving a MIDI file, because MIDI files require
	an 'EndOfTrack' event at the last tick, which we have to add manually.
	"""
	@classmethod
	def get_last_event_tick(self):
		"""
		We want the last tick out of all 8 tracks, so we need to
		compare the last ticks of all tracks to determine which one is the latest.
		"""
		lastTick = 0
		for t in range(8):
			events = self.pattern[t]
			events.sort()
			if len(events) == 0:
				continue
			if events[len(events)-1].tick > lastTick:
				lastTick = events[len(events)-1].tick
		return lastTick

	"""
	Called during system startup to play a quick scale up and down the keyboard.
	This is just to prove that the MIDI connection is working.
	It coincides with the demo 'lightshow' (pulsing of panel LEDs and scale on keyboard LEDs).
	"""
	@classmethod
	def play_demo_scale(self):
		events = []
		curTick = 20
		lenTicks = 8
		for oct in range(5):
			start = 36 + (oct*12)
			end = start + 12
			for notenum in range(start, end):
				events.append(midi.NoteOnEvent(tick=curTick, pitch=notenum, velocity=100))
				curTick += lenTicks
				events.append(midi.NoteOnEvent(tick=curTick, pitch=notenum, velocity=0))
				curTick += 1
		self.play_events(events)
				
	"""
	Sends the given list of events to the MIDI output ports for playback.
	"""
	@classmethod
	def play_events(self, events):
		self.seq.drop_output()
		events.sort() # events must be sorted in time, by tick value.
		lastEvent = None

		self.seq.change_tempo(self.tempo)
		self.seq.start_sequencer()
		self.seqStarted = True
		self.busyPlaying = True
		
		"""
		Loop through all the events and send each one to the ALSA sequencer for output.
		The ALSA buffer is checked so that we don't send more events if the buffer is full.
		
		ALSA handles the timing for us. As long as the timestamps on our messages are correct,
		we can send them all at once and ALSA will deliver them to the MIDI ports at exactly
		the right time according to their tick timestamps.
		"""	
		for event in events:
			if not self.busyRecording and not self.busyPlaying:
				# maybe playback has been stopped from another thread. We must keep checking.
				break
			lastEvent = event
			self.seqLock.acquire()
	    		buf = self.seq.event_write(event, False, False, True)
			self.seqLock.release()
			if buf == None:
				continue
    			if buf < 1000:
        			time.sleep(.5)
		while (self.busyRecording or self.busyPlaying) and lastEvent is not None and lastEvent.tick > self.seq.queue_get_tick_time():
			"""
			We have sent all the events and now we wait for ALSA to play them all at the correct time.
			Repeated calls to drain() ensure that ALSA delivers all the messages in its queue to the MIDI output ports.
			"""
			self.seq.drain()
    			time.sleep(.5) # Call drain() at 2 Hz.

    	"""
    	When we get here, playback has finished (the latest event we sent has been played).
    	"""
		if not self.busyRecording:
        	# finished with playback and we're not recording, so stop the sequencer:
			self.seq.stop_sequencer()
			self.seqStarted = False
			self.busyPlaying = False

	"""
	Sends a single note to the sequencer for playback, of the given note number and duration.
	This is used by the ear training exercises to play the various notes the learners must listen for.
	"""
	@classmethod
	def play_single_note(self, note_number, duration_seconds):
		tickOffset = 20 # to make sure we play the note if we send it after the sequencer has passed tick 0.
		on = midi.NoteOnEvent(tick=tickOffset, velocity=127, pitch=note_number);
		off = midi.NoteOnEvent(tick=tickOffset + self.seconds_to_ticks(duration_seconds), pitch=note_number, velocity=0)
		self.play_events([on, off])

	"""
	Called by the 'chord matching' exercise to play the chords learners have to choose from.
	"""
	@classmethod
	def play_chord(self, note_numbers, duration_seconds):
		tickOffset = 20 # to make sure we play the chord if we send it after the sequencer has passed tick 0.
		events = []
		for n in note_numbers:
			events.append(midi.NoteOnEvent(tick=tickOffset, velocity=127, pitch=n))
			events.append(midi.NoteOnEvent(tick=tickOffset + self.seconds_to_ticks(duration_seconds), velocity=0, pitch=n))

		self.play_events(events)


	"""
	Called by the rhythm memorization exercise to record the short rhythms played
	by the teacher and learner for comparison.
	"""
	@classmethod
	def record_input_segment(self, numBars):
		self.seq.drain()
		self.seq.stop_sequencer()
		self.seqStarted = False
		
		self.nonTrackMode = True
		self.stop_sequencer()
		self.nonTrackMode = True
		self.nonTrackEvents = []
		# Generate the metronome events for the number of bars
		# plus one count-in bar at the beginning:
		self.metronomeEvents = self.generate_metronome_events(numBars, 1)

		self.metronomeEvents.sort()
		self.stopAfterTick = self.metronomeEvents[-1].tick
		
		self.start_recording()
		
		# The recording thread will return when it reaches the end of the last bar:
		self.recordingThread.join()
		# recording is done
		self.seq.stop_sequencer()
		self.seqStarted = False
		self.busyRecording = False
		self.busyPlaying = False
		self.nonTrackEvents.sort()
		
		"""
		The recorded events are returned to the rhythm exercise class
		so that it can perform the comparison and award points.
		"""
		return self.nonTrackEvents


	"""
	While recording rhythms for the rhythm memorization exercise,
	a metronome click is played as a timing reference.
	This method generates the metronome clicks at the correct distance apart
	according to the tempo and number of bars requested by the exercise class.
	"""
	@classmethod
	def generate_metronome_events(self, numBars, countInMeasures=0):
		events = []
		for bar in range(1, numBars+countInMeasures+1):
			for beat in range(1, self.beatsPerMeasure+1):
				noteNum = 34 if beat == 1 else 32
				onTick = self.tick_for_measure_and_beat(bar, beat)
				offTick = onTick + (float(self.ticksPerBeat) / float(2))
				on = midi.NoteOnEvent(tick=int(onTick+20), velocity=127, pitch=noteNum, channel=9)
				off = midi.NoteOnEvent(tick=int(offTick+20), pitch=noteNum, velocity=0, channel=9)
				events.append(on)
				events.append(off)
		return events


	"""
	Computes the tick number that the given musical measure and beat starts on.
	"""
	@classmethod
	def tick_for_measure_and_beat(self, measure, beat):
		cumulativeBeat = ((measure-1) * self.beatsPerMeasure) + beat
		return cumulativeBeat * self.ticksPerBeat


	@classmethod
	def seconds_to_ticks(self, seconds):
		return int(float(self.tempo * self.RESOLUTION * seconds * 1000) / float(60000))

	"""
	Called when the 'keyboard select' button is pressed (next to the eight track buttons).
	"""
	@classmethod
	def select_keyboard(self, keyboard=0):
		self.selectedKeyboard = keyboard
		self.update_track_leds()

	"""
	Called when the 'keyboard select' button is pressed (next to the eight track buttons).
	"""
	@classmethod
	def toggle_selected_keyboard(self):
		if self.selectedKeyboard == 0:
			self.select_keyboard(1)
		else:
			self.select_keyboard(0)
		if not self.nonTrackMode:
			gui_manager.GUIManager.show_screen('sequencer')

	"""
	Called when one of the eight track buttons is 'long-pressed'.
	This arms or disarms the track for recording on the currently selected keyboard.
	"""
	@classmethod
	def toggle_track_armed(self, trackIndex):
		if self.armedTrackNumbers[self.selectedKeyboard] == trackIndex:
			self.armedTrackNumbers[self.selectedKeyboard] = -1
		else:
			self.armedTrackNumbers[self.selectedKeyboard] = trackIndex
		self.update_track_leds()
		if not self.nonTrackMode:
			gui_manager.GUIManager.show_screen('sequencer')

	"""
	Updates the LEDs above the eight 'track' buttons according to each track's state.
	Also, light up the correct 'keyboard select' LED based on the currently selected keyboard.
	"""
	@classmethod
	def update_track_leds(self):
		PanelLedSender.track_leds_off()
		for t in range(0, 8):
			if t == self.armedTrackNumbers[self.selectedKeyboard]:
				# This track is armed for the current keyboard. Make its led red:
				PanelLedSender.track_led_on(t+1, True) # True means red.
				continue
			if len(self.pattern[t]) > 0:
				# this track has events and is not armed, so make the led green:
				PanelLedSender.track_led_on(t+1, False) # False is green.
			

		PanelLedSender.led_off(PanelLedSender.TRACK_KB_SELECT_A)
		PanelLedSender.led_off(PanelLedSender.TRACK_KB_SELECT_B)
		if self.selectedKeyboard > 0:
			PanelLedSender.led_on(PanelLedSender.TRACK_KB_SELECT_B)
		else:
			PanelLedSender.led_on(PanelLedSender.TRACK_KB_SELECT_A)

	"""
	Called when one of the eight track buttons are pressed (not long-pressed).
	This causes the LCD to show that track's details.
	"""
	@classmethod
	def select_track(self, track_index):
		if not self.nonTrackMode:
			gui_manager.GUIManager.show_screen('sequencer')
			gui_manager.GUIManager.sequencerScreen.select_track(track_index + 1)

	"""
	Saves the current recording as a MIDI file to the given file path.
	The user types the filename in the file chooser dialog (see sequencer_screen.py).

	The 'PythonMIDI' library is used to generate the MIDI file:
	https://github.com/vishnubob/python-midi
	"""
	@classmethod
	def save_pattern(self, path):
		try:
			patternToSave = self.pattern
			lastTick = self.get_last_event_tick()
			patternToSave[0].append(midi.EndOfTrackEvent(tick=lastTick+1))
			patternToSave.make_ticks_rel()
			midi.write_midifile(path, patternToSave)
		except:
			traceback.print_exc()	

	"""
	Loads the MIDI file at the given path, and replaces the current recording
	with the one in the file.
	In future versions, this should have better error handling,
	e.g. if the path is not a valid MIDI file or if it contains too many tracks to handle,
	etc.

	MIDI file decoding is done by the PythonMIDI library:
	https://github.com/vishnubob/python-midi
	"""
	@classmethod
	def load_pattern(self, path):
		try:
			self.pattern = midi.read_midifile(path)
			self.update_track_leds()
		except:
			traceback.print_exc()	

	"""
	The MainController class will call this method whenever a button press
	occurs that is of interest to this class.
	This method calls the appropriate other method based on which button was pressed.
	"""
	@classmethod
	def panel_button_pressed(self, button_number):
		if button_number == PanelButtonReceiver.TRACK_KB_SELECT:
			self.toggle_selected_keyboard()
		if button_number == PanelButtonReceiver.REC:
			self.start_recording()
		if button_number == PanelButtonReceiver.PLAY:
			self.start_playback()
		if button_number == PanelButtonReceiver.STOP:
			Clock.schedule_once(self.stop_sequencer)
		trackIndex = PanelButtonReceiver.button_number_to_track_index(button_number)
		if trackIndex >= 0:
			self.select_track(trackIndex)


	"""
	The MainController class will call this method whenever a button 'long-press'
	occurs that is of interest to this class.
	This method calls the appropriate other method based on which button was pressed.
	"""
	@classmethod
	def panel_button_held(self, button_number):
		trackIndex = PanelButtonReceiver.button_number_to_track_index(button_number)
		if trackIndex >= 0:
			self.toggle_track_armed(trackIndex)
			gui_manager.GUIManager.sequencerScreen.select_track(trackIndex + 1)

		if button_number == PanelButtonReceiver.REC:
			# when holding the record button, a new recording is started:
			if not self.nonTrackMode:
				gui_manager.GUIManager.show_screen('sequencer')
			self.new_recording()

	"""
	This is called whenever a track's volume changes, either through the volume knob
	or the Mute / Solo buttons.
	It sends the latest volumes of each track to the MIDI output port 
	as Expression controller messages.
	"""
	@classmethod
	def send_volume_messages(self):
		for t in range(8):
			try:
				ev = midi.ControlChangeEvent()
				vol = int(self.TRACK_VOLUMES[t])
				if self.any_solo_active() and not self.TRACK_SOLOS[t]:
					vol = 1 # some synthesizers interpret zero as 'return to previous value', so we send 1 instead.
				elif self.TRACK_MUTES[t]:
					vol = 1
				ev.data = [11, int(vol)] # MIDI controller 11 is the Expression controller (alters the volume of a channel)
				ev.channel = int(self.TRACK_CHANNELS[t])
				buf = self.seq.event_write(ev, True, False, False)
			except:
				continue

	"""
	Returns whether any of the tracks are solo'd.
	"""
	@classmethod
	def any_solo_active(self):
		for t in range(8):
			if self.TRACK_SOLOS[t]:
				return True
		return False

	"""
	Called when a track's volume changes by one step, i.e. when the
	rotary encoder has turned one click.
	If 'steps' is positive, the encoder was turned clockwise and the volume
	is increased. If it is negative, the encoder was turned counter-clockwise and the volume
	is decreased.
	"""
	@classmethod
	def increment_volume(self, track, steps):
		# Clamp the volume between 0 and 127, as defined by the MIDI standard.
		if self.TRACK_VOLUMES[track-1] + steps > 127:
			self.TRACK_VOLUMES[track-1] = 127
			self.send_volume_messages()
			return
		if self.TRACK_VOLUMES[track-1] + steps < 0:
			self.TRACK_VOLUMES[track-1] = 0
			self.send_volume_messages()
			return

		self.TRACK_VOLUMES[track-1] += steps
		self.send_volume_messages()
	
	@classmethod
	def set_track_mute(self, track, muted):
		self.TRACK_MUTES[track-1] = muted
		self.send_volume_messages()

	@classmethod
	def set_track_solo(self, track, solo):
		self.TRACK_SOLOS[track-1] = solo
		self.send_volume_messages()

	@classmethod
	def set_volume(self, track, value):
		if value > 127:
			value = 127
		if value < 0:
			value = 0
		self.TRACK_VOLUMES[track-1] = value
		self.send_volume_messages()

	"""
	Called by the SequencerScreen class so that the recorder's
	current status can be shown on the LCD.
	"""
	@classmethod
	def get_status(self):
		if self.busyRecording:
			return 'Recording'
		elif self.busyPlaying:
			return 'Playing'
		else:
			return 'Stopped'
			





