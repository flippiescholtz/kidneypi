import sqlite3 as lite

"""
This class is used to insert and retrieve information from the SQLite database.
The database itself is a file called kidneypi.db.

Each method in this class runs a particular query on the database.
"""
class DbManager:
	
	"""
		Returns a list of all users on the system.
	"""
	@classmethod
	def get_users(self):
		con = lite.connect('kidneypi_python/kidneypi.db')
		with con:
			cur = con.cursor()    
    			cur.execute('SELECT * FROM users;')

    			data = cur.fetchall()
    		users = []

    		for u in data:
    			users.append({'id':u[0],'user_name': u[1], 'archived':u[2]})

    
    		return users

    """
    Returns the user with the given id.
    """
	@classmethod
	def get_user(self, user_id):
    		con = lite.connect('kidneypi_python/kidneypi.db')
		with con:
			cur = con.cursor()    
    			cur.execute('SELECT * FROM users WHERE id = ?;', (user_id,))

    			data = cur.fetchone()
    		return {'id':data[0],'user_name': data[1], 'archived':data[2]}

	
    """
    	Creates a new user in the database with the given username.
    	Returns the id of the new user, or -1 if the username was already taken.
    """
	@classmethod
	def insert_user(self, username):
		con = lite.connect('kidneypi_python/kidneypi.db')
		with con:
 			# check if user already exists:
			cur = con.cursor()
			cur.execute("SELECT id FROM users WHERE LOWER(user_name) = LOWER(?);", (username.lower().strip(),))
			existingUser = cur.fetchall()
			if len(existingUser) > 0:
				return -1

			# User does not exist. Create the record:
			try:
				cur.execute("INSERT INTO users (user_name, archived) VALUES (?, 0)", (username.strip(),))
			except:
				return -1
			return cur.lastrowid

	"""
	Record a single ear training exercise attempt to the database for the given user.
	Returns the id of the record.
	"""
	@classmethod
	def insert_results(self, user_id, exercise_name, level, points, test_mode=False):
		con = lite.connect('kidneypi_python/kidneypi.db')
		with con:
			cur = con.cursor()
			cur.execute("PRAGMA foreign_keys = ON;")
			cur.execute("INSERT INTO results (user_id, exercise_name, level, points, is_test) VALUES (?, ?, ?, ?, ?)", (user_id, exercise_name, level, points, 1 if test_mode else 0))
			return cur.lastrowid

	"""
	Returns the total number of points that a particular user (user_id) has achieved
	in a particular type of exercise (exercise_type) in the given mode (test or practice).
	exercise_name can be 'relative_pitch', 'interval', 'chord_matching' or 'rhythm'
	"""
	@classmethod
	def get_points_for_exercise(self, user_id, exercise_name, test_mode=False):
		con = lite.connect('kidneypi_python/kidneypi.db')
		with con:
			cur = con.cursor()
			cur.execute("SELECT SUM(points) as total_points FROM results WHERE user_id = ? AND exercise_name = ? AND is_test = ?", 
				(user_id, exercise_name, 1 if test_mode else 0))
			result = cur.fetchone()[0]
			return result if result is not None else 0

	"""
	Returns the number of attempts that the given user has made on the
	given exercise type, regardless if their answer was correct.
	"""
	@classmethod
	def get_attempts_for_exercise(self, user_id, exercise_name, test_mode=False):
		con = lite.connect('kidneypi_python/kidneypi.db')
		with con:
			cur = con.cursor()
			cur.execute("SELECT COUNT(id) as total_attempts FROM results WHERE user_id = ? AND exercise_name = ? AND is_test = ?", 
				(user_id, exercise_name, 1 if test_mode else 0))
			result = cur.fetchone()[0]
			return result if result is not None else 0

	"""
	Returns the total number of points a user has ever achieved 
	in the particular mode (test or practice)
	"""
	@classmethod
	def get_total_points_for_user(self, user_id, test_mode=False):
		con = lite.connect('kidneypi_python/kidneypi.db')
		with con:
			cur = con.cursor()
			cur.execute("SELECT SUM(points) as total_points FROM results WHERE user_id = ? AND is_test = ?", 
				(user_id, 1 if test_mode else 0))
			result = cur.fetchone()[0]
			return result if result is not None else 0

	"""
	Returns the total number of exercises ever attempted by a particular user
	in a particular mode (test or practice).
	"""
	@classmethod
	def get_total_attempts_for_user(self, user_id, test_mode=False):
		con = lite.connect('kidneypi_python/kidneypi.db')
		with con:
			cur = con.cursor()
			cur.execute("SELECT COUNT(id) as total_attempts FROM results WHERE user_id = ? AND is_test = ?", 
				(user_id, 1 if test_mode else 0))
			result = cur.fetchone()[0]
			return result if result is not None else 0

	"""
	Returns the total number of attempt the given user has ever made
	in the given level (1, 2 or 3) of the particular exercise.
	"""
	@classmethod
	def get_total_attempts_for_level(self, user_id, exercise_name, level, test_mode=False):
		con = lite.connect('kidneypi_python/kidneypi.db')
		with con:
			cur = con.cursor()
			cur.execute("SELECT COUNT(id) as total_attempts FROM results WHERE user_id = ? AND exercise_name = ? AND level = ? AND is_test = ?", 
				(user_id, exercise_name, level, 1 if test_mode else 0))
			result = cur.fetchone()[0]
			return result if result is not None else 0

	"""
	Returns the number of attempts answered correctly by the given user
	in the given exercise category, level and mode.
	"""
	@classmethod
	def get_correct_attempts_for_level(self, user_id, exercise_name, level, test_mode=False):
		con = lite.connect('kidneypi_python/kidneypi.db')
		with con:
			cur = con.cursor()
			cur.execute("SELECT COUNT(id) as total_attempts FROM results WHERE user_id = ? AND exercise_name = ? AND level = ? AND is_test = ? AND points > 0", 
				(user_id, exercise_name, level, 1 if test_mode else 0))
			result = cur.fetchone()[0]
			return result if result is not None else 0

