from panel_led_sender import PanelLedSender
from keyboard_led_sender import KeyboardLedSender
from panel_button_receiver import PanelButtonReceiver
from rotary_encoder_receiver import RotaryEncoderReceiver
from kivy.app import App
from kivy.lang import Builder
from kivy.uix.screenmanager import ScreenManager, Screen
from home_screen import HomeScreen
from kivy.lang import Builder
import threading
from gui_manager import GUIManager
from midi_sequencer import MidiSequencer
import RPi.GPIO as GPIO  
import os
import time
from threading import RLock
from eartraining.training_manager import TrainingManager
from session_manager import SessionManager
import midi_listener

"""
This is the first Python class that starts up.
It is responsible for starting up all other threads in the system.
It acts as a 'dispatcher' of sorts, receiving input events and distributing them
to the other classes that need them.
"""
class MainController:

	@classmethod
	def startup(self):
		"""
		Turns on the screen, starts scanning the keyboard and panel, and puts the system into its initial state.
		"""
		self.started = True

		"""
		Make sure that the UART TX pin is in the UART mode and not GPIO mode.
		We need UART for MIDI.
		"""
		os.system("gpio mode 15 alt0")

		"""
		Initialize and start all the other Python threads that receive input events,
		etc.
		"""
		PanelButtonReceiver.start()
		RotaryEncoderReceiver.start()
		PanelLedSender.start()
		KeyboardLedSender.initialize()
		MidiSequencer.initialize()
		midi_listener.MidiListener.start_listening()
		TrainingManager.initialize(GUIManager.trainingManagerScreen.get_screen_manager())
		SessionManager.logout()
		GUIManager.show_screen('login')
		
		"""
		This call will block as long as the Kivy UI is displayed.
		If this call returns, the system should shut down.
		Currently, shutdown and startup is managed by the two bash scripts
		switch_check.sh and launch.sh, so this call should never return.
		"""
		GUIManager.start()
		
		# GUI call has returned, so GUI is no longer showing. Might as well shut down the system.
		self.shutdown()

	"""
	Called by the MIDIListener class when it receives a Note On event.
	"""
	@classmethod
	def note_on(self, note_number, velocity, keyboard):
		GUIManager.homeScreen.note_on(note_number, velocity, keyboard)

	"""
	Called by the MIDIListener class when it receives a Note Off event.
	"""
	@classmethod
	def note_off(self, note_number, keyboard):
		GUIManager.homeScreen.note_off(note_number, keyboard)
		MidiSequencer.note_off(note_number, keyboard)

	"""
	Called by the PanelButtonReceiver class when it receives
	a panel button pressed message.
	"""
	@classmethod
	def button_pressed(self, button_number):
		# Lock the user out of the panel controls until they log in:
		if not SessionManager.is_logged_in():
			return
		GUIManager.homeScreen.panel_button_pressed(button_number)
		MidiSequencer.panel_button_pressed(button_number)

		"""
		Button events are sent to the appropriate class depending on their function:
		"""
		if button_number == PanelButtonReceiver.SAVE:
			TrainingManager.deactivate()
			GUIManager.show_screen('sequencer')
			GUIManager.sequencerScreen.show_save()
		if button_number == PanelButtonReceiver.LOAD:
			TrainingManager.deactivate()
			GUIManager.show_screen('sequencer')
			GUIManager.sequencerScreen.show_load()
		if button_number == PanelButtonReceiver.LOGOUT:
			TrainingManager.deactivate()
			SessionManager.logout()
			GUIManager.show_screen('login')
		if button_number == PanelButtonReceiver.HOME:
			TrainingManager.deactivate()
			GUIManager.show_screen('home')
		elif button_number == PanelButtonReceiver.START:
			GUIManager.show_screen('training_manager')
			TrainingManager.start_button_pressed()
		elif button_number == PanelButtonReceiver.RESULTS:
			GUIManager.show_screen('training_manager')
			TrainingManager.deactivate()
			TrainingManager.show_results()
		elif button_number == PanelButtonReceiver.LEVEL_NEXT:
			TrainingManager.level_up()
		elif button_number == PanelButtonReceiver.LEVEL_PREV:
			TrainingManager.level_down()
		elif button_number == PanelButtonReceiver.CAT_NEXT:
			TrainingManager.next_category()
		elif button_number == PanelButtonReceiver.CAT_PREV:
			TrainingManager.prev_category()
		else:
			TrainingManager.deactivate()

	"""
	Called by the PanelButtonReceiver class when it receives
	a panel button held ('long-press') message.
	"""
	@classmethod
	def button_held(self, button_number):
		if not SessionManager.is_logged_in():
			return
		GUIManager.homeScreen.panel_button_held(button_number)
		MidiSequencer.panel_button_held(button_number)


	"""
	Called by the RotaryEncoderReceiver class when it receives
	a rotary encoder message. It then sends the event to any classes that need to
	respond to it.
	Currently, the encoder is only used for adjusting the volume of a track on the SequencerScreen.
	"""
	@classmethod
	def rotary_encoder_turned(self, encoder_number, direction):
		if GUIManager.get_current_screen() == 'sequencer' and encoder_number == 1:
			GUIManager.sequencerScreen.increment_volume(direction)

