from kivy.app import App
from kivy.lang import Builder
from kivy.uix.screenmanager import Screen
from panel_button_receiver import PanelButtonReceiver
from panel_led_sender import PanelLedSender
import struct
import zmq
from session_manager import SessionManager

"""
This class performs two functions:
1) Updates the selected instrument information displayed to the user on the LCD.
2) Handles the selection of instrument sounds for the two keyboards, and the setting
of split points.

Basically, it handles the 'performance' features as specified in the report.
"""
class HomeScreen(Screen):

	PART_UPPER1 = 0
	PART_UPPER2 = 1
	PART_LOWER = 2

	"""
	Each of the two keyboards have three instrument parts for which sounds can be chosen:
	Upper1, Upper2 and Lower.
	This array contains the sound indices for each part in the following order:
	Internal Keyboard Upper1, Upper2, Lower; External Keyboard Upper1, Upper2, Lower.
	(3 x 2 = 6 sounds that can be selected.)

	The indices are for the list caleld 'patchNumbers'. 
	"""
	selectedSoundIndices = [0, 1, 3, 0, 1, 3]

	"""
	If a piano key is pressed while this is true,
	the split point of the selected keyboard is changed for that note.
	"""
	waitingForSplitPoint = False

	def initialize(self):
		"""
		When the user selects a sound, the updated sounds list is sent
		to the C++ LiveRouter class using ZeroMQ. Here, we initialize
		the ZeroMQ publisher on TCP port 5557.
		"""
		self.zmqContext = zmq.Context()
		self.patchChangeSocket = self.zmqContext.socket(zmq.PUB)
		self.patchChangeSocket.bind("tcp://*:%s" % 5557)
		
		"""
		These values are the General MIDI instrument numbers for each of the 9
		sound select buttons.
		0 is Piano, 48 is violins, 73 is Flute, etc.
		'-1' means 'Drums'.
		Drums don't have a specific GM instrument number. The notes are just
		sent on MIDI Channel 10. That is handled by the C++ LiveRouter class.
		"""
		self.patchNumbers = [0, 48, 25, 35, 73, -1, 56, 16, 21] # 80 was where -1 (drums) is now.
		self.patchNames = ["Piano", "Strings", "Guitar", "Bass", "Flute", "Drums", "Trumpet", "Organ", "Accordion"]
		
		"""
		When the device starts up, these sounds are selected by default.
		"""
		self.soundIndexUpper1 = 0 # default to piano
		self.soundIndexUpper2 = 1 # default to strings
		self.soundIndexLower = 3 # default to bass
		
		"""
		 0 = internal keyboard, 1 = MIDI keyboard.
		 When the user presses one of the 'sound' buttons,
		 it will change only for this selected keyboard.
		"""
		self.selectedKeyboard = 0 
		self.selectedPart = self.PART_UPPER1
		self.waitingForSplitPoint = False

		"""
		The MIDI note numbers that is the boundary between the 'Upper' and 'Lower'
		parts on each of the two keyboards.
		Both default to the C below middle C (note number 48).
		"""
		self.splitPoints = [48, 48]
		self.update_sound_labels() # Show the selected instrument names on the LCD.
		self.update_part_leds() # Make sure the correct LEDs are lit for the selected part and keyboard.
		self.update_keyboard_leds()
		self.update_midi_patches() # Send the Program Change events to the keyboard.

	def on_show(self):
		self.ids.lbl_username.text = "Welcome, " + SessionManager.get_user_name() + "!"

	"""
	The following two methods allow other classes to get the currently selected 
	'Upper 1' sound on the given keyboard.
	This is used by the MidiSequencer class so that it knows which instrument to record
	based on the user's current selection.
	"""
	def get_upper1_patchnumber(self, keyboard=0):
		return self.patchNumbers[self.selectedSoundIndices[keyboard * 3]]

	def get_upper1_patchname(self, keyboard=0):
		return self.patchNames[self.selectedSoundIndices[keyboard * 3]]


	"""
	This is called by the MainController class when a relevant panel button is pressed.
	It calls the appropriate other method depending on which button was pressed.
	"""
	def panel_button_pressed(self, button_number):
		sound_index = PanelButtonReceiver.button_number_to_sound_index(button_number)
		if sound_index >= 0:
			# One of the 10 'Sound Select' buttons were pressed.
			self.select_sound_at_index(sound_index)
			return
		if button_number == PanelButtonReceiver.PART:
			# This button toggles between the three parts (Upper1, Upper2, Lower)
			self.select_next_part()
			return
		if button_number == PanelButtonReceiver.SOUND_KB_SELECT:
			# This button toggles between the two keyboards.
			self.select_next_keyboard()

	"""
	Cycles between the three parts (upper1, upper2, lower),
	lighting the respective LEDs as it does so.
	"""
	def select_next_part(self):
		if self.selectedPart == self.PART_UPPER1:
			self.select_part(self.PART_UPPER2)
		elif self.selectedPart == self.PART_UPPER2:
			self.select_part(self.PART_LOWER)
		else:
			self.select_part(self.PART_UPPER1)

	"""
	Toggles the selected keyboard between 'internal' and 'external MIDI-connected'.
	"""
	def select_next_keyboard(self):
		self.selectedKeyboard = 1 if self.selectedKeyboard == 0 else 0
		self.update_keyboard_leds()
		self.update_part_leds()	

	"""
	Selects the given part 
	('part' is one of self.PART_UPPER1, self.PART_UPPER2, or self.PART_LOWER)
	"""
	def select_part(self, part):
		self.selectedPart = part
		self.update_part_leds()

	"""
	Turns on the correct LEDs based on the selected sound and part.
	"""
	def update_part_leds(self):
		PanelLedSender.led_off(PanelLedSender.PART_UPPER1)
		PanelLedSender.led_off(PanelLedSender.PART_UPPER2)
		PanelLedSender.led_off(PanelLedSender.PART_LOWER)

		"""
		We want to load the correct sound from the 'selectedSoundIndices' list.
		The internal keyboard's sounds are at indices 0, 1 and 2.
		The external keyboard's sounds are at indices 3, 4 and 5.
		For the internal keyboard, self.selectedKeyboard will be 0,
		and for external keyboard it is 1.
		This allows us to easily calculate the offset in the array we want to start at.
		"""
		kbOffset = self.selectedKeyboard * 3

		if self.selectedPart == self.PART_UPPER1:
			PanelLedSender.led_on(PanelLedSender.PART_UPPER1)
			PanelLedSender.show_sound_led(self.selectedSoundIndices[kbOffset]+1)
		elif self.selectedPart == self.PART_UPPER2:
			PanelLedSender.led_on(PanelLedSender.PART_UPPER2)
			PanelLedSender.show_sound_led(self.selectedSoundIndices[kbOffset + 1]+1)
		else:
			PanelLedSender.led_on(PanelLedSender.PART_LOWER)
			PanelLedSender.show_sound_led(self.selectedSoundIndices[kbOffset + 2]+1)

	"""
	Turns on the correct 'keyboard' LED based on the currently selected keyboard.
	"""
	def update_keyboard_leds(self):
		PanelLedSender.set_led(PanelLedSender.SOUND_KBD_INT, self.selectedKeyboard == 0)
		PanelLedSender.set_led(PanelLedSender.SOUND_KBD_EXT, self.selectedKeyboard == 1)


	"""
	This is called when the 'Part select' button is held down.
	It causes the 'Lower' LED to flash, and waits for the user
	to press the key on the keyboard where they want the split point to be.
	"""		
	def ask_for_split_point(self):
		PanelLedSender.start_flashing_led(PanelLedSender.PART_LOWER)
		self.waitingForSplitPoint = True

	"""
	Called when the user enters a new split point from the keyboard.
	Updates the split point for the currently selected keyboard to the given note number.
	"""
	def update_split_point(self, note):
		self.waitingForSplitPoint = False
		self.splitPoints[self.selectedKeyboard] = note
		PanelLedSender.stop_flashing_led(PanelLedSender.PART_LOWER)
		self.update_part_leds()
		self.update_midi_patches()

	"""
	Called by the MainController class when a key is pressed
	on one of the piano keyboards.
	"""
	def note_on(self, note, velocity, keyboard):
		if self.waitingForSplitPoint:
			self.update_split_point(note)
			return


	"""
	Shows the list of selected sounds for each keyboard on the LCD screen.
	The corresponding Kivy layout can be found in the file 'home.kv'.
	"""
	def update_sound_labels(self):
		# Internal keyboard:
		self.ids.lbl_kb1_upper1_sound.text =  "Upper 1: " + (self.patchNames[self.selectedSoundIndices[0]] if self.selectedSoundIndices[0] >= 0 else "Off")
		self.ids.lbl_kb1_upper2_sound.text =  "Upper 2: " + (self.patchNames[self.selectedSoundIndices[1]] if self.selectedSoundIndices[1] >= 0 else "Off")
		self.ids.lbl_kb1_lower_sound.text =  "Lower: " + (self.patchNames[self.selectedSoundIndices[2]] if self.selectedSoundIndices[2] >= 0 else "Off")

		# External keyboard:
		self.ids.lbl_kb2_upper1_sound.text =  "Upper 1: " + (self.patchNames[self.selectedSoundIndices[3]] if self.selectedSoundIndices[3] >= 0 else "Off")
		self.ids.lbl_kb2_upper2_sound.text =  "Upper 2: " + (self.patchNames[self.selectedSoundIndices[4]] if self.selectedSoundIndices[4] >= 0 else "Off")
		self.ids.lbl_kb2_lower_sound.text =  "Lower: " + (self.patchNames[self.selectedSoundIndices[5]] if self.selectedSoundIndices[5] >= 0 else "Off")


	"""
	Sends the currently selected MIDI instrument numbers to the C++ LiveRouter class
	using ZeroMQ.
	"""
	def update_midi_patches(self):
		topic = 10001 # Not used, but set it anyway.

		"""
		After the topic, the ZeroMQ message contains 8 integers, namely:
		the six selected General MIDI instrument sounds (3 on each keyboard).
		followed by the note numbers of the two keyboards' split points.

		All of this info is sent in one go whenever one of the parameters changes.
		"""

		messageData = ""
		for i in range(6):
			patch = -2
			if self.selectedSoundIndices[i] >= 0:
				patch = self.patchNumbers[self.selectedSoundIndices[i]]
			messageData += str(patch) + " "

		messageData += str(self.splitPoints[0]) + " "
		messageData += str(self.splitPoints[1])

		self.patchChangeSocket.send("%d %s" % (topic, messageData))


	"""
	This is called when one of the ten 'sound select' buttons are pressed.
	It changes the currently selected keyboard part to that sound.
	"""
	def select_sound_at_index(self, index):
		index = index - 1
		kbOffset = 3 if self.selectedKeyboard == 1 else 0

		if self.selectedPart == self.PART_UPPER1:
			self.selectedSoundIndices[0 + kbOffset] = index
		elif self.selectedPart == self.PART_UPPER2:
			self.selectedSoundIndices[1 + kbOffset] = index
		else:
			self.selectedSoundIndices[2 + kbOffset] = index
		self.update_part_leds()
		self.update_sound_labels()
		self.update_midi_patches()
	
			
	"""
	Called by the MainController class when a 'long-press' event is received on a panel button.
	"""
	def panel_button_held(self, button_number):
		if button_number == PanelButtonReceiver.PART and self.selectedPart == self.PART_LOWER:
			self.ask_for_split_point()
