from main_controller import MainController

"""
This is the main entry point for the Python side of the system.
It just calls into the MainController class, which actually starts everything up.
"""
mainController = MainController()
mainController.boot()