-- This script is used to create the database file. It creates the table structures.
BEGIN;
CREATE TABLE users (id INTEGER PRIMARY KEY, user_name VARCHAR(50));
CREATE TABLE results (id INTEGER PRIMARY KEY, user_id INTEGER, exercise_name VARCHAR(50), level INTEGER, is_test INTEGER, points INTEGER, FOREIGN KEY(user_id) REFERENCES users(id));
COMMIT;