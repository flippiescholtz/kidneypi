import time
import math
import threading
import main_controller
import zmq

"""
This class listens for ZeroMQ events from C++ relating to the
rotary encoders. The encoders are scanned in 'kidneypi_c/RotaryEncoderDriver.cpp'.
This class's 'scan' method runs in a separate thread and receives a ZeroMQ message
every time an encoder is turned.
It then passes the encoder event to any other classes that need to respond to it.
"""
class RotaryEncoderReceiver:
	# If this is set to false, break out of the loop and stop listening.
	_listening = False

	"""
	This is called from the MainController class to start the encoder listening thread.
	"""
	@classmethod
	def start(self):
		print("Starting rotary encoder receiver")
		self.receiverThread = threading.Thread(target=self.listen)
		self._listening = True
		self.receiverThread.start()

	@classmethod
	def stop(self):
		if self._listening:
			self._listening = False
			self.receiverThread.join()

	"""
	This runs in a separate thread, started from the MainController class.
	"""
	@classmethod
	def listen(self):
		# Initialize ZeroMQ and bind to the correct port.
		zmqContext = zmq.Context()
		zmqSocket = zmqContext.socket(zmq.SUB) # This class is the subscriber. The C++ side is the publisher.
		zmqSocket.connect("tcp://127.0.0.1:5560") # The C+ side transmits encoder events on local port 5560.
		topicfilter = "10010" # This is not actually used.
		zmqSocket.setsockopt(zmq.SUBSCRIBE, topicfilter)
		
		print "Waiting for rotary encoder events from c++"

		while self._listening:
			msg = zmqSocket.recv()
			tokens = msg.rstrip(' \t\t\n\0').split()
			# Encoder message received. Notify the MainController which will dispatch it to other classes that need it.
			# tokens[1] is the encoder number, tokens[2] is the direction (1 is clockwise, -1 is counterclockwise).
			main_controller.MainController.rotary_encoder_turned(int(tokens[1]), int(tokens[2]))		





