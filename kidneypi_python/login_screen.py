from kivy.app import App
from kivy.lang import Builder
from kivy.uix.screenmanager import Screen, ScreenManager
from kivy.uix.listview import ListItemButton, ListView
from kivy.adapters.listadapter import ListAdapter
from kivy.uix.textinput import TextInput
from db_manager import DbManager
import gui_manager
from session_manager import SessionManager
from kivy.clock import Clock
from functools import partial
import threading
import os

"""
This kivy screen is shown when the system first boots or when no user
is logged in. It displays a list of all users in the database, so that
one can be selected.
"""
class LoginScreen(Screen):
	users = [] # List of all existing users on the system.

	def __init__(self, **kwargs):
		super(LoginScreen, self).__init__(**kwargs)

		self.reload_users()

	"""
	Called by kivy when this screen is first shown.
	It triggers the loading of existing users from the database.
	"""
	def on_show(self):
		self.ids.txt_username.text = ''
		self.ids.tab_panel.switch_to(self.ids.item_existing_user)
		self.reload_users()
		

	"""
	Load the list of existing users from the database and populate the list
	on the screen with their names.
	"""
	def reload_users(self):
		self.users = DbManager.get_users()

		args_converter = lambda row_index, rec: {'text': rec['user_name'],
                                         'size_hint_y': None,
                                         'height': 55,
					'font_size': '18sp'}

		list_adapter = ListAdapter(data=self.users,
                           args_converter=args_converter,
                           cls=ListItemButton,
                           selection_mode='single',
                           allow_empty_selection=False)

		self.ids.lst_users.adapter=list_adapter

	def existing_user_chosen(self, *args):
		if len(self.ids.lst_users.adapter.selection) == 0:
			return
		loginThread = threading.Thread(target=self.login_existing_user, args=(self.users[self.ids.lst_users.adapter.selection[0].index]['id'],))
		loginThread.start()

	def new_user_chosen(self, *args):
		loginThread = threading.Thread(target=self.create_user, args=(self.ids.txt_username.text,))
		loginThread.start()

	"""
	Adds a new user to the database and creates a directory for them
	where their recorded MIDI files will be stored.
	"""
	def create_user(self, username, *args):
		userId = DbManager.insert_user(username.strip())
		if userId <= 0:
			Clock.schedule_once(partial(gui_manager.GUIManager.show_popup_message, "There is already someone with that name. Please choose a different name.", "Invalid name."))
			return
		os.mkdir("/home/pi/patterns/" + username.lower())
		SessionManager.login(userId)
		Clock.schedule_once(partial(gui_manager.GUIManager.show_screen, 'home'))

	def login_existing_user(self, user_id, *args):
		SessionManager.login(user_id)
		Clock.schedule_once(partial(gui_manager.GUIManager.show_screen, 'home'))



