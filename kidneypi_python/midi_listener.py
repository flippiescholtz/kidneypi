import midi
import main_controller
import threading
import midi.sequencer as sequencer
import time

"""
This class listens for MIDI events from both the internal keyboard and
the externally connected MIDI keyboard.
It notifies any other classes that might be interested in the keyboard event.
The multi-track recorder reads the MIDI port directly, so it does not use this class.

This is used by the ear training exercises when they need to listen for a key press as an answer.
Also, the HomeScreen class uses this to listen for the new 'split point' that is entered
as a key press.
"""
class MidiListener:
	running = True

	"""
	This is called on system startup.
	"""
	@classmethod
	def start_listening(self):
		self.nonTrackMode = False
		self.listenThread = threading.Thread(target=self.listen)
		self.listenThread.start()

	"""
	This runs on a separate thread and keeps listening for incoming MIDI messages.
	"""
	@classmethod
	def listen(self):
		seq = sequencer.SequencerRead(sequencer_name='kidneypi_listen_sequencer')
		hardware = sequencer.SequencerHardware()
		
		inputClientInternal = hardware.get_client("IntKB")
		inputPortInternal = inputClientInternal.get_port("IntKBOut")
		inputClientExternal = hardware.get_client("ttymidi")
		inputPortExternal = inputClientExternal.get_port("MIDI out")

		seq.subscribe_port(inputClientInternal.client, inputPortInternal.port)
		seq.subscribe_port(inputClientExternal.client, inputPortExternal.port)

		while self.running:
			event = seq.event_read()
			if event is None:
				# No incoming MIDI messages. Wait a little and check again.
				time.sleep(0.5)
				continue
			"""
			A MIDI message has been received. Notify the MainController class,
			which will dispatch it to all other classes that need it.
			"""
			if isinstance(event, midi.NoteOnEvent):
				main_controller.MainController.note_on(event.pitch, event.velocity, 0)
			if isinstance(event, midi.NoteOffEvent):
				main_controller.MainController.note_off(event.pitch, event.velocity, 0)
			
