# KidneyPi
by Philippus L. Scholtz


KidneyPi is a musical instrument and ear-training device that can be used with any MIDI keyboard. It was developed for my final-year engineering project (EEE4022) at the University of Cape Town.

The [project report](https://www.dropbox.com/s/3sik22xcj7f6ymv/P_Scholtz%20Report%20Final%20EEE4022S.pdf?dl=0) contains a detailed description of the product and its development.

This repository contains all the source code in the following directories:

- **kidneypi_c**: Low-level drivers for the buttons, keys and LEDs
- **kidneypi_python** : All the application logic and flows for all the ear training exercises.

The .kv files contain the user interface layouts for the LCD screen, written in the Kivy language.