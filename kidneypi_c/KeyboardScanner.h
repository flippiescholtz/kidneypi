#ifndef KEYBOARDSCANNER_H
#define KEYBOARDSCANNER_H

#include <wiringPiI2C.h>
#include <iostream>
#include <cstdlib>
#include <stdio.h>
#include <stdlib.h>
#include <alsa/asoundlib.h>
#include "Libs.h"


#define KBD_COL_DEVICE 0x23
#define KBD_ROW_DEVICE 0x24
#define IODIRA 0x00
#define IODIRB 0x01
#define OLATA 0x14
#define OLATB 0x15
#define GPIOA 0x12
#define GPIOB 0x13
#define GPPUA 0x0C
#define GPPUB 0x0D

class KeyboardScanner{
 private:
 	// keeps track of the state of each note contact
	static int* currentStableState;
	static int* prevStableState; 
	// keeps track of how much time passes between each key's two contacts, so we can calculate the velocity:
	static int* velocityCounters;

	static int fdRows, fdCols;

	static snd_seq_t *seq_handle;
	static int clientId;
	static int portId;

	static void noteOn(int notenumber, int velocity);
	static void noteOff(int notenumber);
	static void updateStableState(int* newState);
	static void printMidiOutputPorts();

	static pthread_t scanningThread;

	static int running;
 public:
 	static void* scan(void*);
 	static void start();
 	static void stop();
 	static void joinAndExit();


 };

#endif