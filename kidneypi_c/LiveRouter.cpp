/*
  This class receives MIDI messages from the internal keyboard and external MIDI keybaord.
  It then routes them to the appropriate MIDI channels based on the enabled parts
  (upper 1, upper2 or lower). 
  For example, if a received Note On message is below the split point,
  it will route the message to the MIDI channel that has the Lower part's instrument.

  This class also receives messages from Python over ZeroMQ, which is how it knows
  what the split point and instruments on the various parts are when it routes.

  Six separate MIDI channels are used for each of the three parts on the two keyboards.
  Incoming notes are routed to one of these channels to make the appropriate intrument sound.
*/

#include "LiveRouter.h"
#include <stdexcept>

  // Six integers: Instrument numbers for Upper1, Upper2 and Lower parts for each of the two keyboards.
	 int* LiveRouter::patchNumbers; 

   // Handle to the ALSA sequencer
	 snd_seq_t* LiveRouter::seq_handle;

   /*
      Handle to the internal MIDI keyboard's virtual output port, 
      as registered in KeyboardScanner.cpp .
    */
	 int LiveRouter::internalInputPortId;

    /*
     Handle to the MIDI out port of ttyMidi 
     (the program that reads the UART hardware and converts it into an ALSA MIDI stream)
    */
   int LiveRouter::externalInputPortId;
   int LiveRouter::internalKbClientId;
   int LiveRouter::externalKbClientId;

   /*
    A virtual MIDI out port through which the routed MIDI messages
    are sent. This is connected to the external MIDI keyboard's input port over ttyMidi,
    so that its tone generator can play the sounds. 
   */
	 int LiveRouter::outputPortId;
 

   // The note number of the split point for each keyboard:
	 int LiveRouter::splitPointInternal;
   int LiveRouter::splitPointExternal;

   // Listens for incoming MIDI messages and routes them.
	 pthread_t LiveRouter::routingThread;

   // Listens for settings update messages from Python over ZeroMQ.
	 pthread_t LiveRouter::patchChangeThread;

	 int LiveRouter::running;

   // Lock that protects the array of instrument patch numbers from concurrent access.
   std::mutex LiveRouter::patchMutex;


/*
  Initializes and connects to the various virtual MIDI ports,
  and then starts listening for MIDI and ZeroMQ messages.
*/
void LiveRouter::start(){
	// connect to the ALSA sequencer:
  if (snd_seq_open(&seq_handle, "default", SND_SEQ_OPEN_DUPLEX, 0) < 0) {
    throw std::runtime_error("LiveRouter: Error opening ALSA sequencer.\n");
  }

  /* Find the existing ALSA MIDI ports for the internal and external keyboards,
   so that we can compare the incoming events' source id's to them.
   This is how we know which keyboard a MIDI message came from.
  */
  snd_seq_addr_t internalKbAddress;
  // The name "IntKB" was defined in KeyboardScanner.cpp when the port was registered.
  snd_seq_parse_address(seq_handle, &internalKbAddress, "IntKB:0");
  internalKbClientId = internalKbAddress.client;

  snd_seq_addr_t externalKbAddress;
  // The name 'ttymidi' is automatically registered when starting the ttymidi program.
  snd_seq_parse_address(seq_handle, &externalKbAddress, "ttymidi:0");
  externalKbClientId = externalKbAddress.client;

  std::cout << "Router located kbd clients. Internal: " << internalKbClientId << " , External: " << externalKbClientId << std::endl;

  /*
    Register the two new input ports through which MIDI events will enter the router.
    These are connected to the keyboard and tone generator ports using the 'aconnect' program
    (see launch.sh).
  */
  snd_seq_set_client_name(seq_handle, "LiveRouter");
    if ((internalInputPortId = snd_seq_create_simple_port(seq_handle, "LiveRouterInternalIn",
              SND_SEQ_PORT_CAP_WRITE|SND_SEQ_PORT_CAP_SUBS_WRITE,
              SND_SEQ_PORT_TYPE_APPLICATION)) < 0) {
       throw std::runtime_error("LiveRouter: Error creating internal input port.\n");
    }

    if ((externalInputPortId = snd_seq_create_simple_port(seq_handle, "LiveRouterExternalIn",
              SND_SEQ_PORT_CAP_WRITE|SND_SEQ_PORT_CAP_SUBS_WRITE,
              SND_SEQ_PORT_TYPE_APPLICATION)) < 0) {
       throw std::runtime_error("LiveRouter: Error creating external input port.\n");
    }
 
 /*
  Register the new virtual MIDI out port over which we will send the routed messages. 
 */
    if ((outputPortId = snd_seq_create_simple_port(seq_handle, "LiveRouterOut",
              SND_SEQ_PORT_CAP_READ|SND_SEQ_PORT_CAP_SUBS_READ,
              SND_SEQ_PORT_TYPE_APPLICATION)) < 0) {
       throw std::runtime_error("LiveRouter: Error creating output port.\n");
    }
  

	patchNumbers = new int[6]; // Upper1, Upper2 and Lower patches for 2 keyboards.
  // The numbers are General MIDI instrument (patch) numbers.
  // Defaults on startup:
	patchNumbers[0] = 0; // piano in Upper1 part.
	patchNumbers[1] = 48; // strings mixed with the piano (Upper2)
	patchNumbers[2] = 35; // bass guitar on Lower part.
  patchNumbers[3] = 0;  // Same startup defaults for both keyboards.
  patchNumbers[4] = 48;
  patchNumbers[5] = 35;
	
	splitPointInternal = 48; // By default, the keyboard is split at the second 'C' from the bottom.
  splitPointExternal = 48;
	running = 1;

  // Make sure the MIDI keyboard knows which parts are set to which instruments.
  // See chapter 2.3.3 of the report for an overview of MIDI messages.
  sendProgramChangeEvents();


	std::cout << "Starting threads...\n";
	pthread_create (&routingThread, NULL, &route, NULL); 
	pthread_create (&patchChangeThread, NULL, &listenForPatchChanges, NULL); 

  // Now all action moves to the route() and listenForPatchChanges() functions.
}

void LiveRouter::joinAndExit(){
	void *status;
	pthread_join(routingThread, &status);
	pthread_join(patchChangeThread, &status);

	delete patchNumbers;
}

void LiveRouter::stop(){
	running = 0;
}

/*
  Sends the currently selected instrument sounds
  to the external MIDI keyboard as program change messages.
  This ensures that the correct instrument sounds will play
  when the Note On and Note Off messages are sent.
*/
void LiveRouter::sendProgramChangeEvents(){
    // Build a new Program Change message:
    snd_seq_event_t ev;
    snd_seq_ev_clear(&ev);
    snd_seq_ev_set_subs(&ev);  
    snd_seq_ev_set_direct(&ev);
    snd_seq_ev_set_source(&ev, outputPortId);
    

    /*
      There are six MIDI channels: three parts each assigned to the two keyboards.
      Generate and send Program Change messages for each one.
    */

    // Upper1 on the internal keyboard:
    if(patchNumbers[0] >= 0){
      ev.type = SND_SEQ_EVENT_PGMCHANGE;
      ev.data.control.channel = CHANNEL_INT_UPPER1;
      ev.data.control.value = patchNumbers[0];
      snd_seq_event_output_direct(seq_handle, &ev);
    }

     // Upper2 on the internal keyboard:
    if(patchNumbers[1] >= 0){
      ev.data.control.channel = CHANNEL_INT_UPPER2;
      ev.data.control.value = patchNumbers[1];
      snd_seq_event_output_direct(seq_handle, &ev);
    }

     // Lower on the internal keyboard:
    if(patchNumbers[2] >= 0){
      ev.data.control.channel = CHANNEL_INT_LOWER;
      ev.data.control.value = patchNumbers[2];
      snd_seq_event_output_direct(seq_handle, &ev);
    }

    // Upper1 on the external keyboard:
    if(patchNumbers[3] >= 0){
      ev.data.control.channel = CHANNEL_EXT_UPPER1;
      ev.data.control.value = patchNumbers[3];
      snd_seq_event_output_direct(seq_handle, &ev);
    }

    // Upper2 on the external keyboard:
    if(patchNumbers[4] >= 0){
      ev.data.control.channel = CHANNEL_EXT_UPPER2;
      ev.data.control.value = patchNumbers[4];
      snd_seq_event_output_direct(seq_handle, &ev);
    }

    // Lower on the external keyboard:
    if(patchNumbers[5] >= 0){
      ev.data.control.channel = CHANNEL_EXT_LOWER;
      ev.data.control.value = patchNumbers[5];
      snd_seq_event_output_direct(seq_handle, &ev);
    }
}

/*
  Called whenever there are messages waiting on one of the input ports.
*/
void LiveRouter::routeMidiEvent(){
  snd_seq_event_t *ev;
  do {
    // Connect to the ALSA input stream and read an event.
    snd_seq_event_input(seq_handle, &ev);
    snd_seq_ev_set_subs(ev);  
    snd_seq_ev_set_direct(ev);

    int client = ev->source.client; // so we can figure out which MIDI port the message came from.
    
    if ((ev->type != SND_SEQ_EVENT_NOTEON) && (ev->type != SND_SEQ_EVENT_NOTEOFF)) {
    	// We only care about Note On and Note off mesages. Ignore this one.
      continue;
    }

    // Tell the message that it will go out on our MIDI output port.
     snd_seq_ev_set_source(ev, outputPortId);

     /*
        Look up the General MIDI patch number (instrument)
        that this message should sound on.
        If it is negative, we know that part is turned off on the user interface
        and we shouldn't send the message on our output port.
     */
     int offset = client == internalKbClientId ? 0 : 3;
     int patchUpper1 = patchNumbers[0 + offset];
     int patchUpper2 = patchNumbers[1 + offset];
     int patchLower = patchNumbers[2 + offset];

     /*
      The client ID tells us which input port the message came on,
      i.e. which keyboard the note was played on.
      From this, we determine the MIDI channel number over which the message must be sent.
      Each keyboard has a separate MIDI channel for each of its three parts.
     */
     int channelUpper1 = client == internalKbClientId ? CHANNEL_INT_UPPER1 : CHANNEL_EXT_UPPER1;
     int channelUpper2 = client == internalKbClientId ? CHANNEL_INT_UPPER2 : CHANNEL_EXT_UPPER2;
     int channelLower = client == internalKbClientId ? CHANNEL_INT_LOWER : CHANNEL_EXT_LOWER;

     // Use either the internal keyboard or external keyboard's split point based on the MIDI message's source:
     int splitPoint = client == internalKbClientId ? splitPointInternal : splitPointExternal;

    /*
      Now we will actually send out the MIDI message on the appropriate channel,
      but only if the particular part is turned on.
    */
    patchMutex.lock();
     if(patchLower >= -1 && ev->data.note.note < splitPoint){
     	// We have a lower sound selected and the note is below the split point:
     	 ev->data.note.channel = patchLower >= 0 ? channelLower : 9; // if patch is -1, use percussion.
     	    snd_seq_event_output_direct(seq_handle, ev);
     	    snd_seq_free_event(ev);
              patchMutex.unlock();
     	    continue;
     }

     if(patchUpper1 >= -1){
     	  ev->data.note.channel = patchUpper1 >= 0 ? channelUpper1 : 9; // 9 is percussion
     	    snd_seq_event_output_direct(seq_handle, ev);
     }

     if(patchUpper2 >= -1){
     	  ev->data.note.channel = patchUpper2 >= 0 ? channelUpper2 : 9; // 9 is percussion
     	    snd_seq_event_output_direct(seq_handle, ev);
     }

     patchMutex.unlock();

     snd_seq_free_event(ev); // clean up after ourselves
  } while (snd_seq_event_input_pending(seq_handle, 0) > 0);
}

/*
  This is run on a separate thread. It keeps polling the virtual MIDI input port,
  processing messages as they arrive.
*/
void* LiveRouter::route(void*){
  /*
  ALSA gives us the incoming MIDI messages over 'poll descriptors'.
  Each stream of MIDI data has its own poll descriptor which can be read from.
  We only have one poll descriptor, but we have to go through the motions
  to access it:
  */
  int numPollDescriptors;
    struct pollfd *pollFileDescriptors;

    // Get the number of poll descriptors on the port (should be 1):
    numPollDescriptors = snd_seq_poll_descriptors_count(seq_handle, POLLIN);

    // Allocate memory for the poll descriptor information:
    pollFileDescriptors = (struct pollfd *)alloca(numPollDescriptors * sizeof(struct pollfd));

    // Get a handle to the poll descriptor:
    snd_seq_poll_descriptors(seq_handle, pollFileDescriptors, numPollDescriptors, POLLIN);

    /*
    Now that we have a MIDI poll descriptor, we can keep querying it
    for incoming MIDI messages and process them as they arrive:
    */
    while (running) {
      if (poll(pollFileDescriptors, numPollDescriptors, 100000) > 0) {
          routeMidiEvent();
      }   
    }
  pthread_exit(NULL);
  
  return 0;
}

/*
  This runs on a separate thread, listening for messages from the Python program
  over ZeroMQ.
  These messages can:
    - change the split point for either keyboard
    - change the instrument sound for any of the six keyboard parts
*/
void* LiveRouter::listenForPatchChanges(void*){
    // Connect to the ZeroMQ TCP port that Python is sending on, i.e. 5557:
    zmq::context_t context(1);
    zmq::socket_t subscriber (context, ZMQ_SUB);
    subscriber.connect("tcp://localhost:5557");
    // "10001" is an arbitrarily chosen 'topic id'. 5 is the length, in bytes of the string "10001".
    subscriber.setsockopt( ZMQ_SUBSCRIBE, "10001", 5);

    while (running) {
        /*
          Fetch a new ZeroMQ message.
          This call blocks until a message arrives:
        */
        std::string contents = s_recv (subscriber);

	     // Split the received message into tokens:
        std::istringstream f(contents);
        std::string token;
        int currentTokenNumber = 0;
	       patchMutex.lock();
	     while (getline(f, token, ' ')) {
	       if (currentTokenNumber == 0){
          // First token is the topic (address). Ignore it.
		        currentTokenNumber++;
		        continue; 
	       }

        if(currentTokenNumber == 7){
          // token 7 is the split point for the internal keyboard:
          splitPointInternal = atoi(token.c_str());
        }else if(currentTokenNumber == 8){
          // this is the split point for the external keyboard:
          splitPointExternal = atoi(token.c_str());
        }else{
            // This token contains the a patch number for one of the six parts:
           patchNumbers[currentTokenNumber-1] = atoi(token.c_str());
        }
         // Process the next token...
	       currentTokenNumber++;
       }

  /*
   Some instrument assignments might have changed above, 
   so we re-send the program change messages for each part
   to make sure they are up to date: 
  */
	sendProgramChangeEvents();
	patchMutex.unlock();
  }

    return 0;
  }