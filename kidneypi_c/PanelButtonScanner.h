#ifndef PANELBUTTONSCANNER_H
#define PANELBUTTONSCANNER_H

#include <wiringPiI2C.h>
#include <iostream>
#include <cstdlib>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include "Libs.h"
#include "zmqhelpers.hpp"


// I2C address and internal register addresses of the MCP23017 I/O expander
// connected to the panel button matrix.
#define BTN_DEVICE 0x21
#ifndef IODIRA
#define IODIRA 0x00
#define IODIRB 0x01
#define OLATA 0x14
#define OLATB 0x15
#define GPIOA 0x12
#define GPIOB 0x13
#define GPPUA 0x0C
#define GPPUB 0x0D
#endif

#define BUTTON_DEBOUNCE_THRESHOLD_TICKS 2
#define BUTTON_HELD_THRESHOLD_TICKS 10 

class PanelButtonScanner{
 private:
 	// keeps track of the state of each note contact
	static std::vector<int> currentStableState;
	static std::vector<int> prevStableState; 
	static std::vector<int> btnHeldCounters;
	static std::vector<int> btnHeldFlags;
	static int fdDevice;

	

	static void updateStableState(std::vector<int> &newState);
	static void pressed(int btnIndex);
	static void held(int btnIndex);

	static pthread_t scanningThread;

	static int running;

	static zmq::context_t zmqContext;
    static zmq::socket_t zmqPublisher;
 public:
 	static void* scan(void*);
 	static void start();
 	static void stop();
 	static void joinAndExit();


 };

#endif







