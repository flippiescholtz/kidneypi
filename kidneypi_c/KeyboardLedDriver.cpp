/*
This class does the multiplexing (scanning) of the piano keyboard LED matrix.
It starts two threads:
	midiListenThread: Listens on an ALSA virtual MIDI port for Note On / Note Off messages.
					  Each message updates the state of one key's LED in a state vector.
	
	scanningThread:   Loop through each row in the LED matrix, multiplexing between them.
					  For each row, the appropriate columns are lit up based on the state vector.
					  This happens sufficiently fast that the LEDs appear to be on continuously.

See chapter 5.7 of the project report.

*/

#include "KeyboardLedDriver.h"
#include <stdexcept>

using namespace std;

 // Handles to the MCP23017 I/O expander devices accessed over I2C.
 // There are 23 separate I/O lines across 3 expander devices.
 std::vector<int> KeyboardLedDriver::deviceHandles;

 // 3 ints per row. Each indicates the state of device 2 port A, device 2 port B and device 3 port B, respectively.

/*
This vector contains the current binary value of each I/O expander port for each row.
As MIDI messages arrive, these states are updated so that the multiplexing code can
just send the correct integers to each expander port instead of re-calculating the
binary value every time.
For every row, the nested vector contains the following sequence of integers:
	State of Device 2, Port A
	State of Device 2, Port B
	State of Device 3, Port B
Together, these three ports connect to all 23 matrix lines, as seen in the circuit diagram in the report.
*/
 std::vector<std::vector<int>> KeyboardLedDriver::portStatesPerRow(3, std::vector<int>(3, 0));

 // MIDI messages are received through a dedicated ALSA sequencer client and port,
 // both registered by this class.
 snd_seq_t* KeyboardLedDriver::seq_handle;
 int KeyboardLedDriver::inputClientId;
 int KeyboardLedDriver::inputPortId;

 int KeyboardLedDriver::running;

 pthread_t KeyboardLedDriver::scanningThread; // does the row multiplexing
 pthread_t KeyboardLedDriver::midiListenThread; // receives Note On and Note Off messages

 void KeyboardLedDriver::start(){
 	
 	// initialize the expander ports for each row so that the correct cathode line is pulled low for each one.
 	portStatesPerRow[0][0] = 6; // row 1: bit 0 cleared on device 2 port A
 	portStatesPerRow[1][0] = 5; // row 2: bit 1 cleared on device 2 port A
 	portStatesPerRow[2][0] = 3; // row 3: bit 2 cleared on device 2 port A
 	// From here onwards, we'll do some bit twiddling to set / clear appropriate column bits
 	// whenever we turn an led on or off.

 	// Connect to the I2C devices (the two MCP23017 IC's)
 	deviceHandles = std::vector<int>(2, 0);
 	deviceHandles[0] = wiringPiI2CSetup(0x22);
 	deviceHandles[1] = wiringPiI2CSetup(0x23);
	
	// All LED matrix lines are outputs. Set the MCP23017 data direction registers accordingly:
	wiringPiI2CWriteReg8(deviceHandles[0], IODIRA, 0x00);
	wiringPiI2CWriteReg8(deviceHandles[0], IODIRB, 0x00);
	wiringPiI2CWriteReg8(deviceHandles[1], IODIRB, 0x00);

	// Connect to the ALSA sequencer.
	if(snd_seq_open(&seq_handle, "hw", SND_SEQ_OPEN_DUPLEX, 0) < 0){
		throw std::runtime_error("KeyboardLedDriver: Error opening the Alsa sequencer.");
	}
	snd_seq_set_client_name(seq_handle, "IntKBLeds");	

	// Register a new virtual MIDI port on which we will receive Note On / Note Off events:
	inputPortId = snd_seq_create_simple_port(seq_handle, "IntKBLedsIn", SND_SEQ_PORT_CAP_WRITE | SND_SEQ_PORT_CAP_SUBS_WRITE, SND_SEQ_PORT_TYPE_APPLICATION);
	if(inputPortId < 0){
		throw std::runtime_error("KeyboardLedDriver: Error opening the Alsa port.");
	}
	cout << "KeyboardLedDriver initialized.\n";

	
	running = 1;

	std::cout << "Starting keyboard LED multiplexing thread...\n";
	pthread_create (&scanningThread, NULL, &scan, NULL); 
	pthread_create(&midiListenThread, NULL, &listenForMidi, NULL);

	// Now all the action moves to the scan() and listenForMidi() functions.
}

/*
	This gets called whenever a MIDI message is received.
	It updates the MCP23017 port state vectors for the appropriate row,
	so that the change will appear on the LEDs as the multiplexing thread works.
*/
 void KeyboardLedDriver::setLed(int gmNoteNumber, int state){
 	int noteIndex = gmNoteNumber - 36; // our keboard's lowest note is GM note 36
 	
 	// Look up the bit that needs to be set, and on which device / port / row:
 	int portIndex = CONNECTIONS[noteIndex][0];
 	int columnBit = CONNECTIONS[noteIndex][1];
 	int rowBit = CONNECTIONS[noteIndex][2];

 	if(state == 1){
 		// turn the LED on by setting the appropriate column bit:
 		portStatesPerRow[rowBit][portIndex] |= 1 << columnBit;
 	}else{
 		// turn the LED off by clearing the appropriate column bit:
 		portStatesPerRow[rowBit][portIndex] &= ~(1 << columnBit);
 	}
 }

 void KeyboardLedDriver::ledOn(int gmNoteNumber){
 	setLed(gmNoteNumber, 1);
 }

 void KeyboardLedDriver::ledOff(int gmNoteNumber){
 	setLed(gmNoteNumber, 0);
 }

void KeyboardLedDriver::joinAndExit(){
	void *scanStatus;
	void *midiStatus;
	pthread_join(scanningThread, &scanStatus);
	pthread_join(midiListenThread, &midiStatus);
}

void KeyboardLedDriver::stop(){
	running = 0;
}

/*
	Performs the row multiplexing for the LED matrix.
	It pulls each row low and then writes the correct port values for that row
	to each I/O expander.
*/
void* KeyboardLedDriver::scan(void*)
{
	do {
		
		// loop through each row in our state vector and put the ports into that state for each row:
		for(int r = 0; r < 3; r++){
			wiringPiI2CWriteReg8(deviceHandles[0], OLATA, ~7);
			wiringPiI2CWriteReg8(deviceHandles[0], OLATB, ~portStatesPerRow[r][1]); 
			wiringPiI2CWriteReg8(deviceHandles[1], OLATB, ~portStatesPerRow[r][2]);
			wiringPiI2CWriteReg8(deviceHandles[0], OLATA, ~portStatesPerRow[r][0]); 

			Libs::sleepMillis(10); // scan at 100 Hz.
		}
	}while(running);
	return 0;
}

/*
 Called when any MIDI messages are waiting on our virtual ALSA port.
 It determines whether each message is a Note On or Note off,
 and calls the appropriate function.
*/
void KeyboardLedDriver::handleMidiEvent(){
	 snd_seq_event_t *ev; // pointer to the current MIDI message.

	do {
		// Read the next MIDI message from our virtual MIDI port's queue:
    	snd_seq_event_input(seq_handle, &ev);
    	snd_seq_ev_set_subs(ev);  
    	snd_seq_ev_set_direct(ev);

    	if ((ev->type != SND_SEQ_EVENT_NOTEON) && (ev->type != SND_SEQ_EVENT_NOTEOFF)) {
    		// We only care about Note On and Note Off messages, so ignore this one.
    		continue;
    	}

    	if(ev->type == SND_SEQ_EVENT_NOTEON && ev->data.note.velocity > 0){
    		// Note on. Turn on the led.
    		ledOn(ev->data.note.note);
    	}else{
    		ledOff(ev->data.note.note);
    	}
     	snd_seq_free_event(ev); // clean up after ourselves.
  	} while (snd_seq_event_input_pending(seq_handle, 0) > 0); // are there more messages in the port queue?
}

/*
	Starts listening for MIDI messages on our virtual MIDI port.
*/
void* KeyboardLedDriver::listenForMidi(void*)
{
	/*
	ALSA gives us the incoming MIDI messages over 'poll descriptors'.
	Each stream of MIDI data has its own poll descriptor which can be read from.
	We only have one poll descriptor, but we have to go through the motions
	to access it:
	*/
	int numPollDescriptors;
  	struct pollfd *pollFileDescriptors;

  	// Get the number of poll descriptors on the port (should be 1):
  	numPollDescriptors = snd_seq_poll_descriptors_count(seq_handle, POLLIN);

  	// Allocate memory for the poll descriptor information:
  	pollFileDescriptors = (struct pollfd *)alloca(numPollDescriptors * sizeof(struct pollfd));

  	// Get a handle to the poll descriptor:
  	snd_seq_poll_descriptors(seq_handle, pollFileDescriptors, numPollDescriptors, POLLIN);


  	/*
		Now that we have a MIDI poll descriptor, we can keep querying it
		for incoming MIDI messages and process them as they arrive:
  	*/
  	while (running) {
    	if (poll(pollFileDescriptors, numPollDescriptors, 100000) > 0) {
      		handleMidiEvent();
    	}	  
	}
	pthread_exit(NULL);
	
	return 0;
}