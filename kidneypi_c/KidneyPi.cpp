#include "KidneyPi.h"
#include <iostream>
#include "KeyboardScanner.h"
#include "LiveRouter.h"
#include "PanelButtonScanner.h"
#include "KeyboardLedDriver.h"
#include "RotaryEncoderDriver.h"
#include "Libs.h"

using namespace std;

/*
	The entry point for the C++ side of things.
	It starts up all the various multiplexing threads for the I/O devices,
	as well as the live MIDI routing.
*/
int main(int argc, char *argv[])
{
	cout << "Starting main program...\n";
	
	KeyboardScanner::start();
	PanelButtonScanner::start();
	KeyboardLedDriver::start();
	RotaryEncoderDriver::start();
	LiveRouter::start();
	cout << "Router started.\n";

	
	/*
		I know this is bad practice.
		The code should listen for a SIGTERM event from the operating system
		and exit gracefully.
		At the moment, everything is just killed by the OS whenever the 
		'power' switch is turned off.
	*/ 
	while(1){
		Libs::sleepMillis(5000);
	}

	return 0;
}