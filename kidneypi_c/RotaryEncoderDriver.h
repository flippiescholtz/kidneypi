#ifndef ROTARY_ENCODER_DRIVER_H
#define ROTARY_ENCODER_DRIVER_H

#include <wiringPi.h>
#include <iostream>
#include "zmqhelpers.hpp"
#include "Libs.h"


#define ENC1_A 23 // header pin 29
#define ENC1_B 25 // header pin 31
#define ENC2_A 2  // header pin 13
#define ENC2_B 0  // header pin 11
#define ENC3_A 21 // header pin 33
#define ENC3_B 22 // header pin 37

class RotaryEncoderDriver {
 private:
 	
	static std::vector<int> currentState;
	static std::vector<int> prevState; 

	static pthread_t scanningThread;

	static int running;

	static zmq::context_t zmqContext;
    static zmq::socket_t zmqPublisher;

    static void encoderMoved(int encoderNumber, int direction);
 public:
 	static void* scan(void*);
 	static void start();
 	static void stop();
 	static void joinAndExit();


 };

#endif







