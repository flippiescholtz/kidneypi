/*
	This class performs the multiplexed read of the front panel buttons.
	It works in a similar way to the piano keyboard (KeyboardScanner.cpp).

	It loops through each row in the button matrix, pulling each one high in turn.
	For each row, it checks which columns are high. It performs some debouncing,
	and then sends a ZeroMQ message for any button that has been pressed, held or released.
*/

#include "PanelButtonScanner.h"
#include <stdexcept>


/*
Current and previous state of the entire button matrix.
We compare the current to the previous state so that we know
which buttons have been pressed or released since the last iteration.
*/ 
 std::vector<int> PanelButtonScanner::currentStableState;
 std::vector<int> PanelButtonScanner::prevStableState; 

/*
	These vectors are used to detect 'long presses' of buttons.
	If a button has been pressed and not released for a certain
	amount of time, a 'held' message is sent.
*/ 
std::vector<int> PanelButtonScanner::btnHeldCounters;
std::vector<int> PanelButtonScanner::btnHeldFlags;

// Handle to the MCP23017 device on the I2C bus to which the button matrix is connected.
 int PanelButtonScanner::fdDevice;

 int PanelButtonScanner::running;

 pthread_t PanelButtonScanner::scanningThread;

 // Mechanisms for sending the ZeroMQ messages to Python.
  zmq::context_t PanelButtonScanner::zmqContext(1);
  zmq::socket_t PanelButtonScanner::zmqPublisher(zmqContext, ZMQ_PUB);


/*
Performs some initialization and then starts the multiplexed matrix reading.
*/
 void PanelButtonScanner::start(){

 	// Button events travel over arbitrarily chosen TCP port number 5558:
    zmqPublisher.bind("tcp://127.0.0.1:5558");

    // Set up the I/O expander device (MCP23017):
 	fdDevice = wiringPiI2CSetup(BTN_DEVICE);
	wiringPiI2CWriteReg8(fdDevice, IODIRB, 0x00); // Port B is output:
	wiringPiI2CWriteReg8(fdDevice, IODIRA, 0xFF);  // Port A is an input with pull-up resistors
	wiringPiI2CWriteReg8(fdDevice, GPPUA, 0xFF);

	// Allocate memory for the various vectors. There are 42 buttons on the front panel.
    currentStableState = std::vector<int>(42, 0);
    prevStableState = std::vector<int>(42, 0);
    btnHeldCounters = std::vector<int>(42, 0);
    btnHeldFlags = std::vector<int>(42, 0);

	std::cout << "PanelButtonScanner initialized.\n";
	running = 1;
	pthread_create (&scanningThread, NULL, &scan, NULL); 

	// All the action now moves to the scan() function.
}

void PanelButtonScanner::joinAndExit(){
	void *status;
	pthread_join(scanningThread, &status);
}

void PanelButtonScanner::stop(){
	running = 0;
}

/*
	This runs on a separate thread and performs the multiplexed scanning
	of the button matrix.
*/
void* PanelButtonScanner::scan(void*)
{
	// This is updated as the matrix is scanned.
	std::vector<int> tempState(42, 0);
	
	int ticksStable = 0; // Used for debouncing. How many iterations have passed since the matrix has changed?
	do {
		int stateChanged = 0; // Keeps track of whether the state changed between scans. For debouncing.
		
		// Pull each column high in turn, and check which rows are active for each one:
		for(int row = 0; row < 7; row++){
			wiringPiI2CWriteReg8(fdDevice, OLATB, ~(1 << row)); // invert because we use pull-up resistors
			int colState = wiringPiI2CReadReg8(fdDevice, GPIOA); // read the columns for this row.
			
			for(int col = 0; col < 6; col++){
				int index = row * 6 + col; // convert 2D matrix locaton to 1D array index.
				
				/*
					For each button, we check whether it is pressed by performing
					a bitwise AND operation on the value read from the column lines:
				*/
				int pressed = !(colState & (1 << col));
				if(pressed != tempState[index]){
					/*
						The state of this button has changed since the last iteration,
						so we reset the debouncing counter and flag the matrix as updated.
					*/
					ticksStable = 0;
					tempState[index] = pressed;
					stateChanged = true;
				}
			}
		}
		/*
		We have now completed a full scan of the matrix. 
		If the state hasn't changed since last time, check whether it exceeds the debounce threshold,
		and if so, notify the outside world.
		If the debounce threshold has not passed, we need to wait for it to make sure.
		*/
		if(!stateChanged){
			// state hasn't changed. Increase the stable counter and check for the debounce threshold:
			ticksStable++;
			if(ticksStable > BUTTON_DEBOUNCE_THRESHOLD_TICKS){
				// Everything is debounced! Tell the world about the new state!
				updateStableState(tempState);
				ticksStable = 0;
			}
		}else{
				// State has changed. Reset the debounce counter:
				ticksStable = 0;
		}
		Libs::sleepMillis(10); // Scan the entire matrix at 100 Hz.
	}while(running);
	return 0;
}

/*
	Called when there is a fresh debounced state of the button matrix.
	From here, the 'long presses' are detected and ZeroMQ messages
	are sent out to the Python program.
*/
void PanelButtonScanner::updateStableState(std::vector<int> &newState){
	prevStableState = currentStableState;
	currentStableState = newState;

	for(int btn = 0; btn < 42; btn++){
		if(currentStableState[btn] && !(btnHeldFlags[btn])){
			// Button is down and counting towards a 'lomg press':
			btnHeldCounters[btn]++;
		}

		if(prevStableState[btn] and !currentStableState[btn]){
			if(!btnHeldFlags[btn]){
				// Button was released before a 'long press' event could get triggered.
				// This was a momentary press, so send a normal 'pressed' message:
				pressed(btn);
			}
			// The button has been released, so reset the 'long press' counter:
			btnHeldCounters[btn] = 0;
			btnHeldFlags[btn] = 0;
			continue;
		}
		if(btnHeldCounters[btn] > BUTTON_HELD_THRESHOLD_TICKS){
			/*
				The button has been held for the 'long press' threshold,
				so send a 'long press' event rather than a normal 'pressed' event:
			*/
			if(!btnHeldFlags[btn]){
				/*
					btnHeldFlags is to prevent 'long press' messages from being sent
					continuously when the threshold is exceeded. It ensures that only
					one message is sent, even if the button is held down for longer.
				*/
				held(btn);
			}
			btnHeldCounters[btn] = 0;
			btnHeldFlags[btn] = 1;
		}
	}
}

/*
	Sends a ZeroMQ message to Python containing the button index,
	followed by '1' to signify 'pressed'.
*/
void PanelButtonScanner::pressed(int btnIndex){
     zmq::message_t message(12);
     snprintf ((char *) message.data(), 12 ,
            "10002 1 %d", btnIndex);
     zmqPublisher.send(message);
}

/*
	Sends a ZeroMQ message to Python containing the button index,
	followed by '2' to signify 'long press'.
*/
void PanelButtonScanner::held(int btnIndex){
	std::cout << "Held button " << btnIndex << std::endl;
	 //  Send message to all subscribers
     zmq::message_t message(12);
     snprintf ((char *) message.data(), 12 ,
            "10002 2 %d", btnIndex);
     zmqPublisher.send(message);
}
