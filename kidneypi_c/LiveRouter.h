#ifndef LIVEROUTER_H
#define LIVEROUTER_H

#include <iostream>
#include <alsa/asoundlib.h>
#include "Libs.h"
#include <pthread.h>
#include <iostream>
#include <sstream>


#include <fcntl.h>
#include <stdio.h>
#include <sys/stat.h>
#include <unistd.h>

#include <mutex>

#include "zmqhelpers.hpp"

/*
	There are six MIDI channels, one for each part on each keyboard.
	INT = INTERNAL for the internal keyboard, EXT = EXTERNAL for the external MIDI keyboard.
	Each of these MIDI channels have the General MIDI patches (instruments) assigned to them
	as selected on the 'Soun Select' buttons on the front panel. 
*/
#define CHANNEL_INT_UPPER1 1
#define CHANNEL_INT_UPPER2 2
#define CHANNEL_INT_LOWER 3

#define CHANNEL_EXT_UPPER1 4
#define CHANNEL_EXT_UPPER2 5
#define CHANNEL_EXT_LOWER 6

class LiveRouter{
 private:
 	// Contains 6 entries: For each keyboard, one patch number each for upper1, upper2 and lower.
 	// If no sound is assigned (the part is turned off), it should be -1.
 	static int* patchNumbers; 

	static snd_seq_t *seq_handle;

	// Keyboard connected via the DIN midi ports:
	static int internalInputPortId;
	static int externalInputPortId;
	static int outputPortId;

	static int internalKbClientId;
	static int externalKbClientId;

	static int splitPointInternal;
	static int splitPointExternal;
	

	static pthread_t routingThread;
	static pthread_t patchChangeThread;

	static int running;

	static std::mutex patchMutex;

	static void routeMidiEvent();

	static void sendProgramChangeEvents();


 public:

 	static void* route(void*);
 	static void* listenForPatchChanges(void*);
 	static void start();
 	static void stop();
 	static void joinAndExit();

 };

#endif