/*
	This class performs the multiplexed read of the internal piano keyboard.
	It loops through the 15 rows of the matrix, pulling each one high in turn.
	It then checks which columns are high for that row and compares it with the
	previous state.

	If a key has been pressed or released, the velocity is calculated and a
	MIDI message is sent on a virtual MIDI port registered by this class.
*/

#include "KeyboardScanner.h"
#include <stdexcept>

using namespace std;

/*
 Current and previous states of the whole keyboard, so that we can determine
 which keys have been pressed or released.
*/
 int* KeyboardScanner::currentStableState;
 int* KeyboardScanner::prevStableState; 

 // keeps track of how much time passes between each key's two contacts, so we can calculate the velocity:
 int* KeyboardScanner::velocityCounters;

/*
	File descriptors through which we access the two I2C devices
	connected to the matrix lines.
*/
 int KeyboardScanner::fdRows, KeyboardScanner::fdCols;

/*
 Handles to the ALSA MIDI sequencer and virtual MIDI port
 so that we can send keyboard events to other parts of the
 application:
*/
 snd_seq_t* KeyboardScanner::seq_handle;
 int KeyboardScanner::clientId;
 int KeyboardScanner::portId;

 int KeyboardScanner::running;

 pthread_t KeyboardScanner::scanningThread;

/*
	Initializes the MIDI port and I2C devices, and then starts the read multiplexing.
*/
 void KeyboardScanner::start(){

 	// Connect to the I/O expanders over I2C so that we can read the keyboard state:
 	fdRows = wiringPiI2CSetup(KBD_ROW_DEVICE);
	fdCols = wiringPiI2CSetup(KBD_COL_DEVICE);
	
	wiringPiI2CWriteReg8(fdCols, IODIRA, 0x00); // Port A (columns) is an output
	wiringPiI2CWriteReg8(fdRows, IODIRA, 0xFF);
	wiringPiI2CWriteReg8(fdRows, IODIRB, 0xFF); //  we'll read the rows, so they are inputs (15 rows = 2 ports)
	// enable pull-up resistors on the inputs:
	wiringPiI2CWriteReg8(fdRows, GPPUA, 0xFF);
	wiringPiI2CWriteReg8(fdRows, GPPUB, 0xFF);
	currentStableState = new int[122]; // 61 * 2 contacts
	prevStableState = new int[122];	
	velocityCounters = new int[61];

	// Register our virtual ALSA MIDI port:
	if(snd_seq_open(&seq_handle, "hw", SND_SEQ_OPEN_DUPLEX, 0) < 0){
		throw std::runtime_error("Error opening the Alsa sequencer.");
	}
	snd_seq_set_client_name(seq_handle, "IntKB");	

	portId = snd_seq_create_simple_port(seq_handle, "IntKBOut", SND_SEQ_PORT_CAP_READ | SND_SEQ_PORT_CAP_SUBS_READ, SND_SEQ_PORT_TYPE_APPLICATION);
	if(portId < 0){
		throw std::runtime_error("Error opening the Alsa port.");
	}
	cout << "KeyboardScanner initialized.\n";

	/*
		Now any other program can listen on the 'IntKBOut' MIDI port
		to get notes from the internal keyboard.
	*/
	
	running = 1;

	std::cout << "Starting keyboard scanning thread...\n";
	pthread_create (&scanningThread, NULL, &scan, NULL); 

	// All the action now moves to the scan() function.
}

void KeyboardScanner::joinAndExit(){
	void *status;
	pthread_join(scanningThread, &status);

	delete currentStableState;
	delete prevStableState;
	delete velocityCounters;
}

void KeyboardScanner::stop(){
	running = 0;
}

void* KeyboardScanner::scan(void*)
{
	do {
		int* newState = new int[122];
		// Pull each column high in turn, and check which rows are active for each one:
		for(int c = 0; c < 8; c++){
			wiringPiI2CWriteReg8(fdCols, OLATA, ~(1 << c)); // invert because we use pull-up resistors
			int rowsLSB = wiringPiI2CReadReg8(fdRows, GPIOA);
			int rowsMSB = wiringPiI2CReadReg8(fdRows, GPIOB);
			int rowStatus = (rowsMSB << 8) + rowsLSB;
		
			for(int r = 0; r < 15; r++){
				newState[r * 8 + c] = !(rowStatus & (1 << r));
			}	
		}
		updateStableState(newState); // newState is now assigned to currentStableState so we don't delete it here.
		Libs::sleepMillis(10);
	}while(running);
	return 0;
}

void KeyboardScanner::updateStableState(int* newState){
	prevStableState = currentStableState;
	currentStableState = newState;

	// Now we check which contacts have changed, and calculate velocity values accordingly:
	for(int row = 0; row < 15; row++){
		for(int col = 0; col < 8; col++){
			if(row == 14 && col > 4){
				continue; // The last row only has switches on the first 4 columns.
			}
			int noteNumber = (8 * (row >> 1)) + col; // The lowest note on the keyboard is 0, highest is 60.
			int isSecondContact = row % 2 != 0; 
			if(velocityCounters[noteNumber] > 0){
				// The first contact has already been triggered. Keep counting while waiting for the second one:
				velocityCounters[noteNumber]++;
			}
			int index = row*8+col; // convert 2D matrix location into 1D array index
			if(currentStableState[index] && !prevStableState[index]){
				if(row == 14){
					// The last row of notes have only one contact (highest notes on the keyboard)
					// We don't wait for a second contact because it will never come.
					// Just send the Note On message over the virtual MIDI port:
					noteOn(noteNumber+36, 100); // keyboard starts at Midi note number 36
					continue;
				}
				// This key has multiple contacts so we need to check for that:
				if(!isSecondContact){
					// First contact. Start counting:
					velocityCounters[noteNumber] = 1;
				}else{
					// Second contact. Calculate velocity and generate a note on event:
					int ticks = velocityCounters[noteNumber]; // time passed between two contacts
					/*
						This velocity calculation was developed through experimentation 
						to generate a musically pleasing result:
					*/
					if(ticks > 35){
						// Clamp the time between contacts.
						ticks = 35;
					}
					
					int velocity = 127 - (((float)ticks-2)/(float)35)*127;

					// Send a Note On message over the MIDI port.
					noteOn(noteNumber+36, velocity);		
				}
			}else if(!currentStableState[index] && prevStableState[index] && !isSecondContact){
				// The first contact of the key has been opened since the last iteration,
				// i.e. the key has been completely released. Send a Note Off message:
				noteOff(noteNumber+36);
			}

		}

	}

}

/*
	Called when a key has been pressed on the keyboard.
	Sends a Note On message over the virtual MIDI port 
	so that any subscribed parties can receive it. 
*/
void KeyboardScanner::noteOn(int noteNumber, int velocity){
	snd_seq_event_t evt;
	snd_seq_ev_clear(&evt);
	snd_seq_ev_set_direct(&evt);
	snd_seq_ev_set_subs(&evt);
	snd_seq_ev_set_noteon(&evt, 0, noteNumber, velocity);
	snd_seq_event_output(seq_handle, &evt);
	snd_seq_drain_output(seq_handle);
}


/*
	Called when a key has been released on the keyboard.
	Sends a Note Off message over the virtual MIDI port 
	so that any subscribed parties can receive it. 
*/
void KeyboardScanner::noteOff(int noteNumber){
	snd_seq_event_t evt;
	snd_seq_ev_clear(&evt);
	snd_seq_ev_set_direct(&evt);
	snd_seq_ev_set_subs(&evt);
	snd_seq_ev_set_noteon(&evt, 0, noteNumber,0);
	snd_seq_event_output(seq_handle, &evt);
	snd_seq_drain_output(seq_handle);
}