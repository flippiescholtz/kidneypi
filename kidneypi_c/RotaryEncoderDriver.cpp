#include "RotaryEncoderDriver.h"

/*
	This class scans the three incremental rotary encoders and sends ZeroMQ messages
	to the Python program whenever one of them is turned a notch.

	See the 'Rotary Encoder' section in chapter 2.5 of the report to learn more
	about how rotary encoders are read.

	Current and previous states of all rotary encoder pins.
	We compare the current to the previous to determine
	when a rotary encoder has moved.

	Note: No debouncing is done in this code, because hardware low-pass filters
	are built into the PCB (see chapter 5.10 of the report).
 */
 std::vector<int> RotaryEncoderDriver::currentState;
 std::vector<int> RotaryEncoderDriver::prevState; 

 int RotaryEncoderDriver::running;

 pthread_t RotaryEncoderDriver::scanningThread;


  zmq::context_t RotaryEncoderDriver::zmqContext(1);
  zmq::socket_t RotaryEncoderDriver::zmqPublisher(zmqContext, ZMQ_PUB);


/*
	Connects to the ZeroMQ port and initializes the GPIO pins
	to the correct modes.
*/
 void RotaryEncoderDriver::start(){

    zmqPublisher.bind("tcp://127.0.0.1:5560");

    currentState = std::vector<int>(6, 0);
    prevState = std::vector<int>(6, 0);

    // Set all rotary encoder GPIO pins to be inputs:
    wiringPiSetup();
 	pinMode(ENC1_A, INPUT); // encoder 1 pin A - header pin 29
 	pinMode(ENC1_B, INPUT); // encoder 1 pin B - header pin 31

    pinMode(ENC2_A, INPUT); // encoder 2 pin A - header pin 13
    pinMode(ENC2_B, INPUT); // encoder 2 pin B - header pin 11

    pinMode(ENC3_A, INPUT); // encoder 3 pin A - header pin 33
    pinMode(ENC3_B, INPUT); // encoder 3 pin B - header pin 37

	running = 1;

	std::cout << "Starting rotary encoder scanning thread...\n";
	pthread_create (&scanningThread, NULL, &scan, NULL); 

	// All action has now moved to the scan() function.
}

void RotaryEncoderDriver::joinAndExit(){
	void *status;
	pthread_join(scanningThread, &status);
}

void RotaryEncoderDriver::stop(){
	running = 0;
}

/*
	Called whenever one of the encoders has turned a notch in either direction.
	encoderNumber: a number between 1 and 3 indicating which encoder was moved.
	direction: 1 for clockwise, -1 for anti-clockwise.
*/
void RotaryEncoderDriver::encoderMoved(int encoderNumber, int direction){
	 zmq::message_t message(11);
     snprintf ((char *) message.data(), 11 ,
            "10010 %d %d", encoderNumber, direction);
     zmqPublisher.send(message);
}

void* RotaryEncoderDriver::scan(void*)
{

/*
	Rotary encoder pins form a Gray code as they are turned.
	This program checks for the Gray code by doing the following checks:

	A clockwise movement is detected if any of the following happens:
    B rises while A is high
	A falls while B is high 
	B falls while A is low

	A counter-clockwise movement is detected if any of the following happens:
	B rises while A is low
	A rises while B is high
	B falls while A is high
*/



do{
	prevState = currentState;

	// Read the states of all pins:
	currentState[0] = digitalRead(ENC1_A);
	currentState[1] = digitalRead(ENC1_B);
	currentState[2] = digitalRead(ENC2_A);
	currentState[3] = digitalRead(ENC2_B);
	currentState[4] = digitalRead(ENC3_A);
	currentState[5] = digitalRead(ENC3_B);

	for(int i = 0; i < 6; i += 2){
		// determine which encoder we are busy with (1, 2 or 3):
		int encoderNumber = (i / 2) + 1;

		int curA = currentState[i];
		int prevA = prevState[i];
		int curB = currentState[i+1];
		int prevB = prevState[i+1];

		// check for the clockwise cases:
		if(prevB == 0 && curB == 1 && curA == 1 && prevA == 1){
			encoderMoved(encoderNumber, 1);
			continue;
		}

		if(prevA == 1 && curA == 0 && curB == 1 && prevB == 1){
			encoderMoved(encoderNumber, 1);
			continue;
		}

		if(prevB == 1 && curB == 0 && curA == 0 && prevA == 0){
			encoderMoved(encoderNumber, 1);
			continue;
		}

		// check for the counter-clockwise cases:
		if(prevB == 0 && curB == 1 && curA == 0 && prevA == 0){
			encoderMoved(encoderNumber, -1);
			continue;
		}

		if(prevA == 0 && curA == 1 && curB == 1 && prevB == 1){
			encoderMoved(encoderNumber, -1);
			continue;
		}

		if(prevB == 1 && curB == 0 && curA == 1 && prevA == 1){
			encoderMoved(encoderNumber, -1);
			continue;
		}
	}
		Libs::sleepMillis(2); // Scan at 500 Hz
	}while(running);
	return 0;
}


