#ifndef KEYBOARDLEDDRIVER_H
#define KEYBOARDLEDDRIVER_H

#include <wiringPiI2C.h> // Used to access the I/O expander devices over I2C.
#include <iostream>
#include <cstdlib>
#include <stdio.h>
#include <stdlib.h>
#include "Libs.h"
#include <vector>
#include <alsa/asoundlib.h> // Gives us access to virtual MIDI ports on ALSA.


// Device and register addressses for the I/O expanders (MCP23017 devices)
#define KBD_COL_DEVICE 0x23
#define KBD_ROW_DEVICE 0x24
#define IODIRA 0x00
#define IODIRB 0x01
#define OLATA 0x14
#define OLATB 0x15
#define GPIOA 0x12
#define GPIOB 0x13
#define GPPUA 0x0C
#define GPPUB 0x0D


/*
Out code needs to know which piano key is wired to which I/O device / port.
This array basically encodes the circuit shown in figure 5.8 of the report.

 For each note number (61 of them), the three numbers are:
 {portIndex, columnBit, rowIndex)
 	i.e. For this key, which bit must be set on which port for which row?
    portIndex: (0 = device 2 port A; 1 = device 2 port B, 2 = device 3 port B)
*/
const int CONNECTIONS[61][4] = {
{1,0,2},
{2,1,1},
{2,1,2},
{2,2,1},
{0,6,2},
{0,6,1},
{2,3,1},
{0,6,0},
{1,0,1},
{1,7,2},
{1,0,0},
{1,7,1},
{1,7,0},
{2,3,0},
{0,7,2},
{2,2,0},
{0,7,1},
{0,7,0},
{2,1,0},
{2,7,2},
{0,5,0},
{0,3,2},
{0,4,0},
{0,4,2},
{0,5,2},
{0,3,0},
{1,1,2},
{2,7,0},
{1,1,1},
{1,1,0},
{2,7,1},
{1,2,2},
{0,3,1},
{1,2,1},
{0,4,1},
{1,2,0},
{1,3,2},
{0,5,1},
{1,3,1},
{2,4,2},
{1,3,0},
{1,4,2},
{2,5,2},
{1,4,1},
{2,6,2},
{1,4,0},
{2,6,1},
{1,5,2},
{1,5,1},
{2,5,1},
{1,5,0},
{2,4,1},
{1,6,2},
{1,6,1},
{2,4,0},
{1,6,0},
{2,5,0},
{2,0,2},
{2,6,0},
{2,0,1},
{2,0,0}

};

class KeyboardLedDriver {
 private:

 	static std::vector<int> deviceHandles;

 	// 3 ints per row. Each indicates the state of device 2 port A, device 2 port B and device 3 port B, respectively.
 	static std::vector<std::vector<int>> portStatesPerRow;

	static snd_seq_t *seq_handle;
	static int inputClientId;
	static int inputPortId;

	static void noteOn(int notenumber, int velocity);
	static void noteOff(int notenumber);

	static pthread_t scanningThread;
	static pthread_t midiListenThread;

	static int running;
 public:
 	static void* scan(void*);
 	static void* listenForMidi(void*);
 	static void handleMidiEvent();
 	static void start();
 	static void stop();
 	static void joinAndExit();
 	static void setLed(int gmNoteNumber, int state);
 	static void ledOn(int gmNoteNumber);
 	static void ledOff(int gmNoteNumber);


 };

#endif