#include "Libs.h"
#include <iostream>

/*
	If we write any more utility functions in future, 
	they would go in here.
*/

namespace Libs{

	int sleepMillis(int millis){
		struct timespec tim, tim2;

		tim.tv_sec = millis / 1000;
		tim.tv_nsec = (millis % 1000) * 1000000;
		return nanosleep(&tim, &tim2);
	}

}